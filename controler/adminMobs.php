<?php 
    if(isset($user))
    {
        if($user->admin || $user->superadmin)
        {
            $armesUser = $user->ListerArmes();

            $action = "liste";
            if(isset($_GET["action"]))
            {
                $action = $_GET["action"];
            }
            
            switch($action)
            {
                case "liste" :
                    $mobs = Mob::Lister();
                    $types_mob = Mob::ListerTypesMob();
                    $types_arme = Arme::ListerTypesArme();
                    include("view/mobListe.php");
                break;
                
                case "modifier" :
                    $mob = new Mob($_GET["id"]);
                    $types_mob = Mob::ListerTypesMob();
                    $types_arme = Arme::ListerTypesArme();
                    include("view/mobModifier.php");
                break;
                
                case "ajouter_action" :
                    $types_mob = Mob::ListerTypesMob();
                    $types_arme = Arme::ListerTypesArme();
                    $_POST["type_arme"] = "";
                    foreach($types_arme as $type_arme)
                    {
                        if(isset($_POST[str_replace(" ","_",$type_arme)]))
                        {
                            if($_POST["type_arme"] != "")
                            {
                                $_POST["type_arme"].= '/';
                            }
                            $_POST["type_arme"].= $type_arme;
                        }
                    }
                
                    $mob = new Mob();                    
                    $mob->set_all_from_form($_POST);
                    $mob->ajouter();
                    $mobs = Mob::Lister();
                    
                    include("view/mobListe.php");
                break;
                
                case "supprimer" :
                    $mob = new Mob($_GET["id"]);
                    $types_mob = Mob::ListerTypesMob();
                    if($user->superadmin)
                    {
                        $mob->supprimer();
                    }
                    $mobs = Mob::Lister();
                    $types_arme = Arme::ListerTypesArme();
                    include("view/mobListe.php");
                break;
                
                case "ajouter" :
                    $types_mob = Mob::ListerTypesMob();
                    $types_arme = Arme::ListerTypesArme();
                    include("view/mobAjouter.php");
                break;
                
                case "modifier_action" :
                    $mob = new Mob($_POST["id_mob"]);
                    $types_mob = Mob::ListerTypesMob();
                    $types_arme = Arme::ListerTypesArme();
                    $_POST["type_arme"] = "";
                    foreach($types_arme as $type_arme)
                    {
                        if(isset($_POST[str_replace(" ","_",$type_arme)]))
                        {
                            if($_POST["type_arme"] != "")
                            {
                                $_POST["type_arme"].= '/';
                            }
                            $_POST["type_arme"].= $type_arme;
                        }
                    }
                    
                    $mob->set_all_from_form($_POST);
                    $mob->modifier();
                    $types_arme = Arme::ListerTypesArme();
                    include("view/mobModifier.php");
                break;
            }
        }
        else
        {
            $message = "<span style='color:red;'>Accès refusé.</span>";
            include("view/message.php");
        }
    }
    else
    {        
        $message = "<span style='color:red;'>Accès refusé.</span>";
        include("view/message.php");
    }
    
?>