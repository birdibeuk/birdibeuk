<?php 
    include(__DIR__."/../view/pubs.php");
    if(isset($_POST["id"]))
    {
        
        $user = new User($_POST["id"]);
        
        foreach($_POST as $post=>$value)
        {
            $user->$post = $value;
        }
        
        $userConnected = unserialize($_SESSION["birdibeuk_user"]);
        if($user->id == $userConnected->id || $userConnected->superadmin)
        {
            $user->modifier();
            $user->get_data_from_db($user->id);
            $_SESSION["birdibeuk_user"] = serialize($user);
            include(__DIR__."/../view/editProfil.php");
        }
        else
        {
            include(__DIR__."/../view/404.php");  
        }
    }
    else
    {
        include(__DIR__."/../view/404.php");  
    }
    
?>