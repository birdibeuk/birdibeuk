<?php
    include("view/pubs.php");

    $action = "lister";
    if(isset($_GET["action"]))
    {
        $action = $_GET["action"];
    }

    $ordre = "nom ASC";
    if(isset($_GET["ordre"]))
    {
        $ordre = $_GET["ordre"];
    }

    $nom = "";
    if(isset($_GET["nom"]))
    {
        $nom = $_GET["nom"];
    }

    switch($action)
    {
        case "supprimer":
            $compagnie = new Compagnie($_GET["id_compagnie"]);
            if($user->AAccesACompagnie($compagnie))
            {
                include("view/gestionCompagniesSupprimer.php");
            }
            else
            {
                $message = "Accès refusé.";
                $listeCompagnie = Compagnie::ListerMesCompagnies($ordre, $nom);
                include("view/gestionCompagnies.php");
            }
        break;

        case "actionSuppression":
            $compagnie = new Compagnie($_GET["id_compagnie"]);
            if($user->AAccesACompagnie($compagnie))
            {
                $compagnie->supprimer();
            }
             else
            {
                $message = "Accès refusé.";
            }
            $listeCompagnie = Compagnie::ListerMesCompagnies($ordre, $nom);
            include("view/gestionCompagnies.php");
        break;

        case "actionAjouter":
            $nom = $_POST["nom"];

            $ids = array();
            for($a=1;$a<=$_POST["maxID"];$a++)
            {
                if(isset($_POST["aventurier_".$a]))
                {
                    $ids[] = $a;
                }
            }
            $compagnie = new Compagnie(0,$nom,$user->id);
            foreach($ids as $id)
            {
                $compagnie->ajouteMembre(new Aventurier($id));
            }

            $compagnie->ajouter();
            $listeCompagnie = Compagnie::ListerMesCompagnies();

            include("view/gestionCompagnies.php");
        break;

        case "ajouter":
            $aventuriers = Aventurier::Lister($ordre, $nom);
            $mesAventuriers = Aventurier::ListerMesAventuriers($ordre, $nom);
            include("view/gestionCompagniesAjouter.php");
        break;

        case "visualiser":
            //$aventuriers = Aventurier::Lister($ordre, $nom);
            $compagnie = new Compagnie($_GET["id_compagnie"]);
            include("view/gestionCompagniesVisualiser.php");
        break;

        case "modification":
            $compagnie = new Compagnie($_GET["id_compagnie"]);

            $listeIdMembres = $compagnie->getListeIdMembres();

            //si on a le droit
            if($user->AAccesACompagnie($compagnie))
            {
                $aventuriers = Aventurier::ListerAsArray(" AVENTURIER.NOM ", $nom);
                $mesAventuriers = Aventurier::ListerMesAventuriers($ordre, $nom);
                include("view/gestionCompagniesModifier.php");
            }
            else
            {
                $ordre = "NOM ASC";
                if(isset($_GET["ordre"]))
                {
                    $ordre = $_GET["ordre"];
                }

                $nom = "";
                if(isset($_GET["nom"]))
                {
                    $nom = $_GET["nom"];
                }

                $message = "Accès refusé.";
                $listeCompagnie = Compagnie::ListerMesCompagnies($ordre, $nom);
                include("view/gestionCompagnies.php");
            }
        break;

        case "actionModification":
            $compagnie = new Compagnie($_POST["id_compagnie"]);
            if($user->AAccesACompagnie($compagnie))
            {
                $nom = $_POST["nom"];

                $ids = array();
                for($a=1;$a<=$_POST["maxID"];$a++)
                {
                    if(isset($_POST["aventurier_".$a]))
                    {
                        $ids[] = $a;
                    }
                }
                $compagnie->nom = $nom;
                $compagnie->membres = array();
                foreach($ids as $id)
                {
                    $compagnie->ajouteMembre(new Aventurier($id));
                }

                $compagnie->modifier();
                $listeCompagnie = Compagnie::ListerMesCompagnies();
                include("view/gestionCompagnies.php");
            }
            else
            {
                $ordre = "NOM ASC";
                if(isset($_GET["ordre"]))
                {
                    $ordre = $_GET["ordre"];
                }

                $nom = "";
                if(isset($_GET["nom"]))
                {
                    $nom = $_GET["nom"];
                }

                $message = "Accès refusé.";
                $listeCompagnie = Compagnie::ListerMesCompagnies($ordre, $nom);
                include("view/gestionCompagnies.php");
            }
        break;

        case "lister":
        default :
            $ordre = "NOM ASC";
            if(isset($_GET["ordre"]))
            {
                $ordre = $_GET["ordre"];
            }

            $nom = "";
            if(isset($_GET["nom"]))
            {
                $nom = $_GET["nom"];
            }

            $listeCompagnie = Compagnie::ListerMesCompagnies($ordre, $nom);

            include("view/gestionCompagnies.php");
        break;
    }

?>