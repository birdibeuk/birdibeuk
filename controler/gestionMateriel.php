<?php
  include("view/pubs.php");

  if(isset($_SESSION["birdibeuk_user"]))
  {
      $user = unserialize($_SESSION["birdibeuk_user"]);
  }

  if(!isset($_GET['action']))
  {
      $_GET['action'] = "";
  }

  switch($_GET['action'])
  {
      case 'modifierArme':
        //vérifier que l'arme nous appartient bien
        if($user->PossedeArme($_GET['id'])){
          //accepté
          $arme = new Arme($_GET['id']);
          $types_arme = Arme::ListerTypesArme();
          include("view/gestionMaterielModifierArme.php");
        }else{
          //refusé
          $message = 'Vous n\'avez pas accès à cette arme.';
          $armes = $user->ListerArmes();
          $protections = $user->ListerProtections();
          $equipements = $user->ListerEquipements();

          include("view/gestionMateriel.php");
        }
        break;

      case 'action_modifier_arme':
        //vérifier que l'arme nous appartient bien
        if($user->PossedeArme($_POST['ID'])){
          //accepté
          $arme = new Arme($_POST['ID']);
          $arme->set_all_from_form($_POST);
          $arme->modifier();
          $types_arme = Arme::ListerTypesArme();

          $armes = $user->ListerArmes();
          $protections = $user->ListerProtections();
          $equipements = $user->ListerEquipements();

          include("view/gestionMateriel.php");
        }else{
          //refusé
          $message = 'Vous n\'avez pas accès à cette arme.';
          $armes = $user->ListerArmes();
          $protections = $user->ListerProtections();
          $equipements = $user->ListerEquipements();

          include("view/gestionMateriel.php");
        }
        break;

      default :
        $armes = $user->ListerArmes();
        $protections = $user->ListerProtections();
        $equipements = $user->ListerEquipements();

        include("view/gestionMateriel.php");
        break;
  }