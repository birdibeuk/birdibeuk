<?php     
    
    $user = User::formulaireConnexionValide($_POST["login"], $_POST["password"]);
    
    if($user === false)
    {
        include(__DIR__."/../view/haut_page.php");
		include(__DIR__."/../view/menu.php");
		//erreur
        $messageConnexion = "Erreur, mauvais login ou mot de passe.";
        include(__DIR__."/../view/inscriptionConnexion.php");
    }
    else
    {
        //connecté
        $_SESSION["birdibeuk_user"] = serialize($user); 		
		$_SESSION["birdibeuk_deconnexion"] = false;
		
        $user->updateLastVisit();
        
        header('Location: index.php?ctrl=profil');
        die();
    }
    
    
?>