<?php 
    if(isset($user))
    {
        if($user->admin || $user->superadmin)
        {
            $protectionsUser = $user->ListerProtections();
            
            $action = "liste";
            if(isset($_GET["action"]))
            {
                $action = $_GET["action"];
            }
            
            switch($action)
            {
                case "liste" :
                    $protections = Protection::ListerPourAdmin();
                    $types_protection = Protection::ListerTypesProtection();
                    include("view/protectionListe.php");
                break;
                
                case "modifier" :
                    $protection = new Protection($_GET["id"]);
                    $types_protection = Protection::ListerTypesProtection();
                    include("view/protectionModifier.php");
                break;
                
                case "ajouter_action" :
                    $protection = new Protection();                    
                    $protection->set_all_from_form($_POST);
                    $protection->ajouter();
                    $protections = Protection::ListerPourAdmin();
                    $types_protection = Protection::ListerTypesProtection();
                    include("view/protectionListe.php");
                break;
                
                case "supprimer" :
                    $protection = new Protection($_GET["id"]);
                    if($user->superadmin || in_array($protection,$protectionsUser))
                    {
                        $protection->supprimer();
                    }
                    $protections = Protection::ListerPourAdmin();
                    $types_protection = Protection::ListerTypesProtection();
                    include("view/protectionListe.php");
                break;
                
                case "ajouter" :
                    $types_protection = Protection::ListerTypesProtection();
                    include("view/protectionAjouter.php");
                break;
                
                case "modifier_action" :
                    $protection = new Protection($_POST["ID"]);
                    $protection->set_all_from_form($_POST);
                    $protection->modifier();
                    $types_protection = Protection::ListerTypesProtection();
                    include("view/protectionModifier.php");
                break;
            }
        }
        else
        {
            $message = "<span style='color:red;'>Accès refusé.</span>";
            include("view/message.php");
        }
    }
    else
    {        
        $message = "<span style='color:red;'>Accès refusé.</span>";
        include("view/message.php");
    }
    
?>