<?php     
    include(__DIR__."/config/param.php");
	include(__DIR__."/model/autoloader.php");
	include(__DIR__."/model/GeneralFunctions.php");
    
    $listeUser = User::Lister();
?>
<!doctype html>
<html lang="fr">
	<head>
        <!--[if IE]><link rel="shortcut icon" type="image/x-icon" href="favicon.ico" /><![endif]-->
		<meta charset="utf-8">
		<title>Test API Geocode</title>
		<meta name="description" content="">
		<meta name="author" content="Favay Thomas">
        
        <script type="text/javascript" src="js/jquery.js"></script>        
    </head>
    <body>
        <div id='plan' style='border:1px black solid;width:300px;height:300px;'></div><br>
        <div id='script' style=''></div>
        <script type="text/javascript"
			src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCMBsVRjmJZnCL7x187G8vQvF-mM_87cjI">
		</script>
        <script>
            var adresses = [<?php 
                $compte = 0;
                foreach($listeUser as $user)
                {
                    if($compte > 0)
                    {
                        echo ",";
                    }
                    echo "\"".str_replace('"','\"',$user->localisation)."\"";
                    $compte++;
                }
            ?>];
            
            var id_user  = [<?php 
                $compte = 0;
                foreach($listeUser as $user)
                {
                    if($compte > 0)
                    {
                        echo ",";
                    }
                    echo "\"".$user->id."\"";
                    $compte++;
                }
            ?>];
            
            var indexAdresse = 0;
            var infowindow = new google.maps.InfoWindow();
            
            function getAdress(adresses)
            {
                $.ajax({
                        method: "GET",
                        url: "https://maps.googleapis.com/maps/api/geocode/json",
                        data: { 
                            "address" : adresses[indexAdresse],
                            "key" : "AIzaSyA2x2SomxUTwXyqNn6Yg-pcXWwkuIGYudg"
                        }
                    })  .done(function(result) 
                        {
                            if(result.status == "OK")
                            {
                                $("#script").append(adresses[indexAdresse]+" :: "+result.results[0].geometry.location.lat+', '+result.results[0].geometry.location.lng);
                                
                                $.ajax({
                                    method: "POST",
                                    url: "ajax/modifLatLngUser.php",
                                    data: { 
                                        "id_user" : id_user[indexAdresse],
                                        "latlng" : result.results[0].geometry.location.lat+', '+result.results[0].geometry.location.lng
                                    }
                                })  .done(function(result2) 
                                    {
                                        $("#script").append("::<span style='color:green;'>OK : "+result2+"</span>");
                                        $("#script").append("<br>");
                                    })
                                    .error(function() 
                                    {
                                        $("#script").append("::<span style='color:red;'>NOK</span>");
                                        $("#script").append("<br>");
                                    });
                                //putMarker(result.results[0].geometry.location.lat, result.results[0].geometry.location.lng,adresses[indexAdresse])
                            }
                            
                            if(indexAdresse+1 < adresses.length)
                            {
                                indexAdresse++;
                                getAdress(adresses);
                            }                           
                        })
                        .fail(function() {
                            alert( "error" );
                        });             
            }
            
            function putMarker(lat_, lng_, libelle_)
            {
                var marker = new google.maps.Marker({
                            position: { lat: lat_, lng: lng_ },
                                map: map, 
                                title: libelle_
                            });
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.setContent(libelle_); 
                    infowindow.open(map,marker);
                });
            }
            
            var map;
            function initialize(id) 
            {
                var mapOptions = {
                  center: { lat: 50.397, lng: 4.20},
                  zoom: 4
                };
                map = new google.maps.Map(document.getElementById(id),mapOptions);
                map.setCenter({ lat: 50.397, lng: 4.20});
            }
            
            initialize("plan");
            getAdress(adresses);            
        </script>
    </body>
</html>
