<?php
class Compagnie
{
    //Attributs
    private $id_compagnie;
    private $nom;
    private $id_createur;
    private $date_creation;
    private $membres;
    private $mj;
    private $nbr_aventurier;

    //constructeur
    public function __construct($id_compagnie_envoye = '', $nom_envoye = '', $id_createur_envoye = '', $date_creation_envoye = '')
    {
        $this->membres = array();
        $this->mj = 0;
        $this->nbr_aventurier = 0;
        //si ID
        if(!empty($id_compagnie_envoye) && empty($nom_envoye))
        {
            if(is_array($id_compagnie_envoye))
            {
                $this->set_all_from_form($id_compagnie_envoye);
            }
            else
            {
                $this->id_compagnie = $id_compagnie_envoye;
                $this->get_data_from_db($id_compagnie_envoye);
            }
        }
        else
        {
            $this->id_compagnie = $id_compagnie_envoye;
            $this->nom = $nom_envoye;
            $this->id_createur = $id_createur_envoye;
            $this->date_creation = $date_creation_envoye;
            if($this->id_compagnie != "" && $this->id_compagnie != 0 && $this->id_compagnie != null)
            {
                $this->maj_membres();
            }
        }
    }

    //get
    public function __get($var)
    {
        if($var == "nbr_aventurier")
        {
            return count($this->membres);
        }
        else
        {
            return $this->$var;
        }
    }

    //set
    public function __set($var, $value)
    {
        $this->$var = $value;
    }

    //set all from form
    public function set_all_from_form($post)
    {
        foreach($post as $key=>$value)
        {
            $this->$key = $value;
        }

        if($this->id_createur != 0)
        {
            $this->mj = new User($this->id_createur);
        }
        $this->maj_membres();
    }

    public function maj_membres()
    {
        $db = getConnexionDB();
        $requete = "select * from lien_aventurier_compagnie join aventurier on lien_aventurier_compagnie.id_aventurier = aventurier.ID where id_compagnie = ".$this->id_compagnie;

        $stmt = $db->prepare($requete);
        $stmt->execute();

        while ($rs = $stmt->fetch(PDO::FETCH_ASSOC))
        {
            $this->membres[] = new aventurier($rs['ID'], $rs['NOM'], $rs['SEXE'], $rs['ID_ORIGINE'], $rs['ID_METIER'], $rs['EV'], $rs['EA'], $rs['COU'],
            $rs['INT'], $rs['CHA'], $rs['AD'], $rs['FO'], $rs['AT'], $rs['PRD'], $rs['XP'], $rs['DESTIN'], $rs['OR'], $rs['ARGENT'], $rs['CUIVRE'],
            $rs['NIVEAU'], $rs['idjoueur'], $rs['type'],
            $rs['MAGIEPHYS'],
            $rs['MAGIEPSY'],
            $rs['RESISTMAG'],
            $rs['ID_TYPEMAGIE'],
            $rs['ID_DIEU'],
			array(),
            $rs['PR_MAX'],
            $rs['codeacces'],
            $rs['PR'],
            $rs['bonus_degat'],
            $rs['image_url'],$rs["autre_metier"],$rs["evactuel"],$rs["eaactuel"],$rs["description"]);
        }
    }

    public function print_form()
    {
        ?>
        <table>
            <input type='hidden' id='id_compagnie' value='<?php echo $this->id_compagnie; ?>' name='id_compagnie' />
            <tr><td><label for='nom'>nom</label></td><td><input type='text' id='nom' value='<?php echo $this->nom; ?>' name='nom' /></td></tr>
            <tr><td><label for='id_createur'>id_createur</label></td><td><input type='text' id='id_createur' value='<?php echo $this->id_createur; ?>' name='id_createur' /></td></tr>
            <tr><td><label for='date_creation'>date_creation</label></td><td><input type='text' id='date_creation' value='<?php echo $this->date_creation; ?>' name='date_creation' /></td></tr>
        </table>
        <?php
    }

    public function ajouteMembre($aventurier)
    {
        if(!in_array($aventurier,$this->membres))
        {
            $this->membres[] = $aventurier;
        }
    }

    //liste
    public static function Lister($ordre = "nom ASC",$nom="")
    {
        $ordre = strtolower($ordre);

        $db = getConnexionDB();
        $requete = "SELECT * FROM compagnie";
        if(!empty($nom))
        {
            $requete .= " WHERE aventurier.NOM LIKE '".$nom."%'";
        }
        $requete .= " ORDER BY ".$ordre;

        $tableau = array();
        $stmt = $db->prepare($requete);

        $stmt->execute();

        while ($rs = $stmt->fetch(PDO::FETCH_ASSOC))
        {
            $tableau[] = new Compagnie($rs);
        }
        return $tableau;
    }

    public static function ListerMesCompagnies($ordre = "nom ASC",$nom="")
    {
        $user = unserialize($_SESSION["birdibeuk_user"]);

        if($user->id == null || $user->id == 0)
		{
			return array();
		}
        if($ordre == "")
        {
            $ordre = "nom ASC";
        }
        $ordre = strtolower($ordre);

        $db = getConnexionDB();
        $requete = "SELECT * FROM compagnie where id_createur = ".$user->id;
        if(!empty($nom))
        {
            $requete .= " and compagnie.nom LIKE '".$nom."%'";
        }
        $requete .= " ORDER BY ".$ordre;

        $tableau = array();

        $stmt = $db->prepare($requete);
        $stmt->execute();

        while ($rs = $stmt->fetch(PDO::FETCH_ASSOC))
        {
            $tableau[] = new Compagnie($rs);
        }
        return $tableau;
    }

    public function ajouter()
    {
        $db = getConnexionDB();
        $requete = "INSERT INTO compagnie (nom, id_createur) VALUES ('".str_replace("'","''",$this->nom)."', ".$this->id_createur.")";

        $stmt = $db->prepare($requete);
        $stmt->execute();
        $this->id_compagnie = $db->lastInsertId();

        foreach($this->membres as $membre)
        {
            $requete = "INSERT INTO lien_aventurier_compagnie (id_aventurier, id_compagnie) VALUES (".$membre->ID.",".$this->id_compagnie.")";

            $stmt = $db->prepare($requete);
            $stmt->execute();
        }
    }

    public function modifier()
    {
        $db = getConnexionDB();
        $requete = "UPDATE compagnie SET nom = '".str_replace("'","''",$this->nom)."', id_createur = ".$this->id_createur." WHERE id_compagnie = ".$this->id_compagnie;
        $stmt = $db->prepare($requete);
        $stmt->execute();

        $requete = "DELETE FROM lien_aventurier_compagnie where id_compagnie = :id_compagnie";
        $stmt = $db->prepare($requete);
        $stmt->bindParam(':id_compagnie', $this->id_compagnie);
        $stmt->execute();

        foreach($this->membres as $membre)
        {
            $requete = "INSERT INTO lien_aventurier_compagnie (id_aventurier, id_compagnie) VALUES (".$membre->ID.",".$this->id_compagnie.")";
            $stmt = $db->prepare($requete);
            $stmt->execute();
        }
    }

    public function getListeIdMembres(){
        $tab = array();
        foreach($this->membres as $aventurier){
            $tab[] = $aventurier->ID;
        }
        return $tab;
    }

    public function supprimer()
    {
        $db = getConnexionDB();
        $requete = "DELETE FROM compagnie WHERE id_compagnie = ".$this->id_compagnie;
        $stmt = $db->prepare($requete);
        $stmt->execute();

        $db = getConnexionDB();
        $requete = "DELETE FROM lien_aventurier_compagnie WHERE id_compagnie = ".$this->id_compagnie;
        $stmt = $db->prepare($requete);
        $stmt->execute();
    }

    //get_data_from_db
    public function get_data_from_db($id)
    {
        $db = getConnexionDB();
        $requete = "SELECT * FROM compagnie WHERE id_compagnie = ".$id;
        $stmt = $db->prepare($requete);
        $stmt->execute();

        $ligne = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->id_compagnie = $ligne['id_compagnie'];
        $this->nom = $ligne['nom'];
        $this->id_createur = $ligne['id_createur'];
        $this->date_creation = $ligne['date_creation'];

        $this->maj_membres();
    }

    public function debug()
    {
        echo "<pre>";
        print_r($this);
        echo "</pre>";
    }
}
