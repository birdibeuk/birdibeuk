<?php 
    session_start();
    
	include(__DIR__."/../config/param.php");
	include(__DIR__."/../model/autoloader.php");
	include(__DIR__."/../model/GeneralFunctions.php");
    
    if(isset($_SESSION["birdibeuk_user"]))
    { 
        if($user = unserialize($_SESSION["birdibeuk_user"]))
        {
            if(isset($_POST["id_arme"]))
            {
                if($user->PossedeArme($_POST["id_arme"]))
                {
                    $arme = new Arme($_POST["id_arme"]);
                    $arme->supprimer();
                    echo 1;
                }
                else
                {
                    echo "Accès à l'arme refusé.";
                }
            }
            else
            {
                echo "Arme mal définie.";
            }            
        }
        else
        {
             echo "Accès au script refusé. User invalide.";
        }
    }
    else
    {
        echo "Accès au script refusé. Session corrompue.";
    }
    
?>