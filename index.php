<?php
	session_start();

	// $timestart = microtime(true);

	include(__DIR__."/config/param.php");
	include(__DIR__."/model/autoloader.php");
	include(__DIR__."/model/GeneralFunctions.php");
	include(__DIR__."/model/dice.php");

	if(isset($_GET["ctrl"]))
	{
		if($_GET["ctrl"] == "tentativeConnexion")
		{
			// Création du cookie
			if(isset($_POST["cookie"]))
			{
				$key = sha1('SL1-df546'.$_POST["login"].'SL2-sd55fd');
				setcookie('birdibeuk', $key, time() + 3600 * 24 * 10, '/', 'favay.be', false, true);
			}
			else
			{
				if(isset($_COOKIE['birdibeuk']))
				{
					setcookie('birdibeuk', "vide", time() + 1, '/', 'favay.be', false, true);
				}
			}
		}
	}

	$user = null;
	$rs = false;
	 // Récupération de la valeur du cookie
	if(isset($_COOKIE['birdibeuk']))
	{
		if(isset($_SESSION["birdibeuk_user"]))
		{
			if($_SESSION["birdibeuk_user"] == false)
			{
				if(isset($_SESSION["birdibeuk_deconnexion"]))
				{
					if($_SESSION["birdibeuk_deconnexion"] == false)
					{
						checkCookie($_COOKIE['birdibeuk']);
					}
				}
				else
				{
					checkCookie($_COOKIE['birdibeuk']);
				}
			}
		}
		else
		{
			if(isset($_SESSION["birdibeuk_deconnexion"]))
			{
				if($_SESSION["birdibeuk_deconnexion"] == false)
				{
					checkCookie($_COOKIE['birdibeuk']);
				}
			}
			else
			{
				checkCookie($_COOKIE['birdibeuk']);
			}
		}
	}

    if(isset($_SESSION["birdibeuk_user"]))
    {
        if(isset($_GET["ctrl"]))
        {
            if($_GET["ctrl"] != "deconnexion")
            {
                if($_GET["ctrl"] != "tentativeConnexion")
				{
					include(__DIR__."/view/haut_page.php");
					include(__DIR__."/view/menu.php");
				}

                if($_SESSION["birdibeuk_user"] !== false)
                {
                    if($user = unserialize($_SESSION["birdibeuk_user"]))
                    {
                        if($user->admin || $user->superadmin)
                        {
                            include(__DIR__."/view/menuAdmin.php");
                        }
                    }
                }
            }
        }
        else
        {
            include(__DIR__."/view/haut_page.php");
            include(__DIR__."/view/menu.php");

            if($_SESSION["birdibeuk_user"] !== false)
            {
                if($user = unserialize($_SESSION["birdibeuk_user"]))
                {
                    if($user->admin || $user->superadmin)
                    {
                        include(__DIR__."/view/menuAdmin.php");
                    }
                }
            }
        }
    }
    else  if(isset($_GET["ctrl"]))
    {
        if($_GET["ctrl"] != "tentativeConnexion")
		{
			include(__DIR__."/view/haut_page.php");
			include(__DIR__."/view/menu.php");
		}
    }
    else{
        include(__DIR__."/view/haut_page.php");
		include(__DIR__."/view/menu.php");
    }

    if(isset($_GET["ctrl"]))
	{
		if(is_file(__DIR__."/controler/".$_GET["ctrl"].".php"))
        {
            include(__DIR__."/controler/".$_GET["ctrl"].".php");
        }
        else
        {
            include(__DIR__."/controler/404.php");
        }
	}
	else
	{
		include(__DIR__."/controler/defaut.php");
	}

	include(__DIR__."/view/bas_page.php");
    ?>

