<?php     
    include(__DIR__."/config/param.php");
	include(__DIR__."/model/autoloader.php");
	include(__DIR__."/model/GeneralFunctions.php");
    
    $listeUser = User::Lister();
?>
<!doctype html>
<html lang="fr">
	<head>
        <!--[if IE]><link rel="shortcut icon" type="image/x-icon" href="favicon.ico" /><![endif]-->
		<meta charset="utf-8">
		<title>Test API Geocode</title>
		<meta name="description" content="">
		<meta name="author" content="Favay Thomas">
        
        <script type="text/javascript" src="js/jquery.js"></script>        
    </head>
    <body>
        <?php
        function getXmlCoordsFromAdress($address)
        {
            $coords=array();
            $base_url="http://maps.googleapis.com/maps/api/geocode/xml?";
            // ajouter &region=FR si ambiguité (lieu de la requete pris par défaut)
            $request_url = $base_url . "address=" . urlencode($address).'&sensor=false';
            $xml = simplexml_load_file($request_url) or die("url not loading");
            //print_r($xml);
            $coords['lat']=$coords['lon']='';
            $coords['status'] = $xml->status ;
            if($coords['status']=='OK')
            {
                $coords['lat'] = $xml->result->geometry->location->lat ;
                $coords['lon'] = $xml->result->geometry->location->lng ;
            }
            return $coords;
        }
        
        foreach($listeUser as $user)
        {
            if($user->localisation != "")
            {
                $coords=getXmlCoordsFromAdress($user->localisation);        
                $latlng = $coords['lat'].",".$coords['lon'];
                $user->latlng = $latlng;    
                echo $user->localisation."::".$user->latlng.'<br/>';
                //$user->modifier();
            }
        }
        ?>
    </body>
</html>
