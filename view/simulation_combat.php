<?php 
    $chercher_des_noises = new Competence(14);

    $prioritaires = array();
    
    $ordre = array();
    $indexOrdre = 0;
    
    foreach((array)$aventuriers as $aventurier)
    {
        if($aventurier->possedeCompetence($chercher_des_noises))
        {
            $prioritaires[] = $aventurier;
        }
        else
        {
            $nonPrioritaires[] = $aventurier;
        }
    }
    
    for($cou = 20; $cou>0; $cou--)
    {
        $indexOrdre = count($ordre);
        foreach((array)$prioritaires as $aventurier)
        {
            if($aventurier->COU == $cou)
            {
                $ordre[$indexOrdre][] = $aventurier;
            }
        }
    }     
    
    for($cou = 20; $cou>0; $cou--)
    {
        $indexOrdre = count($ordre);
        
        foreach((array)$nonPrioritaires as $aventurier)
        {
            if($aventurier->COU == $cou)
            {
                $ordre[$indexOrdre][] = $aventurier;
            }
        }
        foreach((array)$mobs as $mob)
        {
            if($mob->cou == $cou)
            {
                $ordre[$indexOrdre][] = $mob;
            }
        }
    }
?>
<style>
    p
    {
        margin-top: 0px;
        margin-bottom: 0px;
    }
</style>
<div class='principal_avec_pub'>
    <table style='margin:auto'>
        <tr>
            <td style='vertical-align:top;'>
                <div style='float:left;width:250px;border:1px #900000 solid;background-image: url("image/bg3.png");'>
                    <?php 
                        foreach($aventuriers as $aventurier)
                        {
                            $arme = $aventurier->armes[0];
                            if($arme->type == "arc")
                            {
                                if(count($aventurier->armes) > 1)
                                {
                                    $arme = $aventurier->armes[1];
                                }
                            }
                            echo "<table>";
                                echo "<tr><td colspan='2' style='text-align:center;'  id='nom_aventurier_".$aventurier->ID."'><b>".$aventurier->NOM."</b></td></tr>";
                                echo "<tr><td>COU:</td><td id='cou_aventurier_".$aventurier->ID."' >".$aventurier->COU."</td></tr>";
                                echo "<tr><td>EV MAX :</td><td>".$aventurier->EV."</td></tr>";
                                echo "<tr><td>EV :</td><td id='ev_aventurier_".$aventurier->ID."' >".$aventurier->EV."</td></tr>";
                                echo "<tr><td>Arme : </td><td id='arme_aventurier_".$aventurier->ID."'>".$arme->NOM."</td></tr>"; 
                                echo "<tr><td>AT : </td><td id='at_aventurier_".$aventurier->ID."'>".($aventurier->AT + $arme->AT )."</td></tr>";
                                echo "<tr><td>PRD: </td><td id='prd_aventurier_".$aventurier->ID."'>".($aventurier->PRD + $arme->PRD  )."</td></tr>";                                                               
                                echo "<tr><td>PI: </td><td id='pi_aventurier_".$aventurier->ID."'>";                                
                                echo $aventurier->armes[0]->PI;
                                if($aventurier->FO > 12)
                                {
                                    echo "+".($aventurier->FO-12);
                                }
                                else if($aventurier->FO < 9)
                                {
                                    echo "-1";
                                }
                                echo "</td></tr>";
                                echo "<tr><td>PR: </td><td id='pr_aventurier_".$aventurier->ID."'>".$aventurier->PR."</td></tr>";
                            echo "</table>";
                        }
                    ?>
                </div>
            </td>
            <td>
                <div style='text-align:center;width:400px;border:1px #900000 solid;padding:10px;background-image: url("image/bg3.png");'><u>Résumé du combat</u>
                    <p style='text-align:center'> Ordre d'attaque : <br>
                    <?php 
                        $compte = 1;
                        foreach($ordre as $key=>$value)
                        {                            
                            foreach($value as $aventurier)
                            {                                
                                if(get_class($aventurier) == "Aventurier")
                                {
                                    echo $compte." : ".$aventurier->NOM."<br>";
                                    $compte++;
                                }
                                else
                                {
                                    echo $compte." : ".$aventurier->nom."<br>";
                                    $compte++;
                                }
                            }
                        }
                    ?>
                    </p>
                    <p>Le combat commence !</p>
                    <div style='text-align:center;'>
                        <input type='button' onclick='a_qui_le_tour();' value='Continuer' />
                        <input type='button' onclick='finir_combat();' value='Voir le résultat' />
                        <a href='index.php?ctrl=outilsMJ&action=creation_simulation_combat&mob=<?php echo $mob->id_mob;?>' ><input type='button' value='Relancer le combat' /></a>
                    </div>
                    <div id='texte_combat'></div>                    
                </div>
            </td>
            <td style='vertical-align:top;'>
            <div style='float:right;width:250px;border:1px #900000 solid;background-image: url("image/bg3.png");'>
                <?php 
                    foreach($mobs as $mob)
                    {
                        echo "<table>";
                            echo "<tr><td colspan='2' style='text-align:center;' id='nom_mob_".$mob->id_mob."'><b>".$mob->nom."</b></td></tr>";
                            echo "<tr><td>COU :</td><td id='cou_mob_".$mob->id_mob."' >".$mob->cou."</td></tr>";
                            echo "<tr><td>EV MAX :</td><td >".$mob->ev."</td></tr>";
                            echo "<tr><td>EV :</td><td id='ev_mob_".$mob->id_mob."' >".$mob->ev."</td></tr>";
                            echo "<tr><td>Arme :</td><td id='arme_mob_".$mob->id_mob."' >".$mob->arme."</td></tr>";
                            echo "<tr><td>AT : </td><td id='at_mob_".$mob->id_mob."'>".$mob->at."</td></tr>";
                            echo "<tr><td>PRD: </td><td id='prd_mob_".$mob->id_mob."'>".$mob->prd."</td></tr>";
                            echo "<tr><td>PI : </td><td id='pi_mob_".$mob->id_mob."'>".$mob->pi."</td></tr>";
                            echo "<tr><td>PR : </td><td id='pr_mob_".$mob->id_mob."'>".$mob->pr."</td></tr>";
                        echo "</table>";
                    }
                ?>
                </div>  
            </td>
        </tr>
    </table>
</div>
<script>
    var combattant_actuel = 99999999;
    var tour_actuel = 0;
    
    var ordre_combattant = [<?php 
        $compte = 0;
        foreach($ordre as $key=>$value)
        {
            foreach($value as $aventurier)
            {                                
                if($compte>0)
                {
                    echo ",";
                }
                if(get_class($aventurier) == "Aventurier")
                {
                    echo '"aventurier_'.$aventurier->ID.'"';
                }
                else
                {
                    echo '"mob_'.$mob->id_mob.'"';
                }
                $compte++;
            }
        }
    ?>];
    var aventurier = [<?php 
        $compte = 0;
        foreach($aventuriers as $aventurier)
        {                                
            if($compte>0)
            {
                echo ",";
            }
            echo '"aventurier_'.$aventurier->ID.'"';           
            $compte++;
        }
    ?>];
    
    var aventurierAfk = [<?php 
        $compte = 0;
        foreach($aventuriers as $aventurier)
        {                                
            if($compte>0)
            {
                echo ",";
            }
            echo '0';           
            $compte++;
        }
    ?>];
    
    var aventurierDesarme = [<?php 
        $compte = 0;
        foreach($aventuriers as $aventurier)
        {                                
            if($compte>0)
            {
                echo ",";
            }
            echo '0';           
            $compte++;
        }
    ?>];
    
    var ennemi = [<?php
        $compte = 0;
        foreach($mobs as $mob)
        {                                
            if($compte>0)
            {
                echo ",";
            }
            echo '"mob_'.$mob->id_mob.'"';           
            $compte++;
        }
    ?>];
    
    au_finish=0;
    
    function finir_combat()
    {
        au_finish=1;
        a_qui_le_tour();
    }
    
    function a_qui_le_tour()
    {
        if(tour_actuel == 0 || (parseInt($("#ev_"+ordre_combattant[combattant_actuel]).html()) > 2 && parseInt($("#ev_"+ordre_combattant[id_autre_combattant()]).html()) > 2))
        {
            combattant_actuel++;
            if(combattant_actuel >= ordre_combattant.length)
            {
                combattant_actuel = 0;
                tour_actuel++;
                
                texte_combat = '<div id="assaut_'+parseInt(tour_actuel)+'" style="font-family:dauphin;margin-top:10px;border:1px #900000 solid;padding:5px;background-image:url(\'image/bg2.png\');" ></div>';
                
                if(au_finish)
                {
                    $("#texte_combat").html(($("#texte_combat").html() + texte_combat));
                }
                else
                {
                    $("#texte_combat").html((texte_combat + $("#texte_combat").html()));
                }
                
            
                //$("#texte_combat").append("<br>");
                //$("#texte_combat").append("<p><u>Tour "+tour_actuel+"</u></p>"); 
                
                $("#assaut_"+parseInt(tour_actuel)).append("<p style='text-align:center;'><b><u style='font-size:26px;'>Assaut "+tour_actuel+"</u></b></p>");                
                $("#assaut_"+parseInt(tour_actuel)).append("<p style='text-align:center;'>"+$("#nom_"+ordre_combattant[combattant_actuel]).html() +" : "+$("#ev_"+ordre_combattant[combattant_actuel]).html()+ "/"+$("#nom_"+ordre_combattant[id_autre_combattant()]).html()+" : "+$("#ev_"+ordre_combattant[id_autre_combattant()]).html()+"</p>");                
            }
            
            $("#assaut_"+parseInt(tour_actuel)).append("<p style='text-align:center;margin-top:10px;'>Initiative : <b>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+"</b>");
            
            if(aventurierAfk[combattant_actuel] > 0)
            {
                aventurierAfk[combattant_actuel] = aventurierAfk[combattant_actuel]-1;
                if(aventurierAfk[combattant_actuel] == 0)
                {
                    //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" se releve.</p>");
                    $("#assaut_"+parseInt(tour_actuel)).append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" se releve.</p>");   
                }
                else
                {
                    //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" tente de se relever.</p>");
                    $("#assaut_"+parseInt(tour_actuel)).append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" tente de se relever.</p>");  
                }
                
                if(au_finish)
                {
                    a_qui_le_tour();
                }
            }
            else
            {
                scoreAT = Math.floor(Math.random()*20 + 1);

                //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" tente une attaque et fait un score de "+scoreAT+".</p>");
                $("#assaut_"+parseInt(tour_actuel)).append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" tente une attaque et fait un score de <b>"+scoreAT+"</b>.</p>");
                if(scoreAT == 1)
                {
                    //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" réussi un coup critique ! Il ne peut pas être paré !</p>");
                    $("#assaut_"+parseInt(tour_actuel)).append("<p><b>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" réussi un coup critique ! Il ne peut pas être paré !</b></p>");
                
                    scoreCritique = Math.floor(Math.random()*20 + 1);
                    degat_calcule = calcule_degat();
                    
                    degat_calcule = gere_attaque_critique(scoreCritique,degat_calcule);
                    
                    inflige_degat(degat_calcule,1); 
                }
                else if(scoreAT == 20)
                {
                    //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" fait un echec critique !</p>");
                    $("#assaut_"+parseInt(tour_actuel)).append("<p><b>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" fait un echec critique !</b></p>");
                
                    scoreCritique = Math.floor(Math.random()*20 + 1);
                    degat_calcule = calcule_degat();
                    
                    degat_calcule = gere_maladresse_critique(scoreCritique,degat_calcule);                
                }
                else if(scoreAT <= $("#at_"+ordre_combattant[combattant_actuel]).html())
                {
                    //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" réussi son attaque.</p>");
                    $("#assaut_"+parseInt(tour_actuel)).append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" réussi son attaque.</p>");
                    
                    if(aventurierAfk[id_autre_combattant()] > 0)
                    {
                        //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[id_autre_combattant()]).html()+" ne peut pas parer car il est a terre.</p>");
                        $("#assaut_"+parseInt(tour_actuel)).append("<p>"+$("#nom_"+ordre_combattant[id_autre_combattant()]).html()+" ne peut pas parer car il est a terre.</p>");
                        degat_calcule = calcule_degat();
                        inflige_degat(degat_calcule,0);
                    }
                    else
                    {
                        scorePRD = Math.floor(Math.random()*20  + 1);
                        //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[id_autre_combattant()]).html()+" tente une parade et fait un score de "+scorePRD+".</p>");
                        $("#assaut_"+parseInt(tour_actuel)).append("<p>"+$("#nom_"+ordre_combattant[id_autre_combattant()]).html()+" tente une parade et fait un score de "+scorePRD+".</p>");
                        if(scorePRD == 1)
                        {
                            //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[id_autre_combattant()]).html()+" fait une réussite critique à la parade !</p>");
                            $("#assaut_"+parseInt(tour_actuel)).append("<p><b>"+$("#nom_"+ordre_combattant[id_autre_combattant()]).html()+" fait une réussite critique à la parade !</b></p>");
                            gere_parade_critique(Math.floor(Math.random()*20  + 1),0);
                        }
                        else if(scorePRD == 20)
                        {
                            //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[id_autre_combattant()]).html()+" fait un echec critique à la parade !</p>");
                            $("#assaut_"+parseInt(tour_actuel)).append("<p><b>"+$("#nom_"+ordre_combattant[id_autre_combattant()]).html()+" fait un echec critique à la parade !</b></p>");
                            gere_parade_maladresse(Math.floor(Math.random()*20  + 1),0);
                        }
                        else if(scorePRD <= $("#prd_"+ordre_combattant[id_autre_combattant()]).html())
                        {
                            //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[id_autre_combattant()]).html()+" réussi sa parade.</p>");
                            $("#assaut_"+parseInt(tour_actuel)).append("<p>"+$("#nom_"+ordre_combattant[id_autre_combattant()]).html()+" réussi sa parade.</p>");                            
                        }
                        else
                        {
                            //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[id_autre_combattant()]).html()+" rate sa parade.</p>");
                            $("#assaut_"+parseInt(tour_actuel)).append("<p>"+$("#nom_"+ordre_combattant[id_autre_combattant()]).html()+" rate sa parade.</p>");
                            degat_calcule = calcule_degat();
                            inflige_degat(degat_calcule,0);
                        }
                    }
                }
                else
                {
                    //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" rate son attaque.</p>");
                    $("#assaut_"+parseInt(tour_actuel)).append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" rate son attaque.</p>");                            
                }
                
                if(au_finish)
                {
                    a_qui_le_tour();
                }
            }
            //$("#texte_combat").append(texte_combat);
        }
        else
        {
            //$("#texte_combat").append("<p>Le combat est terminé, un des deux opposant est hors d'état de combattre.</p>");
        }
    }
    
    function gere_parade_critique(scoreCritique,degats_calc)
    {
        if(scoreCritique == 1 || scoreCritique == 2)
        {
            //$("#texte_combat").append("<p>Mais non en fait, la parade était normale !</p>");        
            $("#assaut_"+parseInt(tour_actuel)).append("<p>Mais non en fait, la parade était normale !</p>");        
        }
        else if(scoreCritique == 3 || scoreCritique == 4 || scoreCritique == 5)
        {
            if(combattant_actuel == 0)
            {
                aventurierAfk[combattant_actuel] = 2;
            }
            else
            {
                aventurierAfk[combattant_actuel] = 1;
            }
            
            //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" est repoussé et tombe !</p>");
            $("#assaut_"+parseInt(tour_actuel)).append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" est repoussé et tombe !</p>");
        }
        else if(scoreCritique == 6 || scoreCritique == 7)
        {
            //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" trébuche et "+$("#nom_"+ordre_combattant[id_autre_combattant()]).html()+" en profite pour frapper !</p>");
            $("#assaut_"+parseInt(tour_actuel)).append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" trébuche et "+$("#nom_"+ordre_combattant[id_autre_combattant()]).html()+" en profite pour frapper !</p>");
            degats_calc = calcule_degat_ennemi();
            inflige_self_degat(degats_calc);
        }
        else if(scoreCritique == 8 || scoreCritique == 9)
        {
            //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" tombe et "+$("#nom_"+ordre_combattant[id_autre_combattant()]).html()+" en profite pour le frapper deux fois !</p>");
            $("#assaut_"+parseInt(tour_actuel)).append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" tombe et "+$("#nom_"+ordre_combattant[id_autre_combattant()]).html()+" en profite pour le frapper deux fois !</p>");
            degats_calc = calcule_degat_ennemi();
            inflige_self_degat(degats_calc);
            degats_calc = calcule_degat_ennemi();
            inflige_self_degat(degats_calc);
        }
        else if(scoreCritique == 10 || scoreCritique == 11 || scoreCritique == 12)
        {
            if($("#arme_"+ordre_combattant[combattant_actuel]).html() != "Mains nues")
            {
                //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" lache son arme !</p>");
                $("#assaut_"+parseInt(tour_actuel)).append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" lache son arme (AT-4, PRD=0, PI=1D-2)!</p>");
                aventurierDesarme[combattant_actuel] = Math.floor(Math.random()*4 + 1);;
                $("#at_"+ordre_combattant[combattant_actuel]).html(parseInt(parseInt($("#at_"+ordre_combattant[combattant_actuel]).html())-4.0));
                $("#prd_"+ordre_combattant[combattant_actuel]).html(0);
                $("#pi_"+ordre_combattant[combattant_actuel]).html("1D-2");            
                $("#arme_"+ordre_combattant[combattant_actuel]).html("Mains nues");   
            }
            else
            {
                $("#assaut_"+parseInt(tour_actuel)).append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" devrait lacher son arme mais n'en a pas, le tout est donc sans effet (les critiques mains nues ne sont pas encore implementés.)!</p>");
            }
        }
        else if(scoreCritique == 13 || scoreCritique == 14 || scoreCritique == 15)
        {
            if($("#arme_"+ordre_combattant[combattant_actuel]).html() != "Mains nues")
            {
                //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" casse son arme !</p>");
                $("#assaut_"+parseInt(tour_actuel)).append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" casse son arme (AT-4, PRD=0, PI=1D-2)!</p>");
                aventurierDesarme[combattant_actuel] = -1;
                
                $("#at_"+ordre_combattant[combattant_actuel]).html(parseInt(parseInt($("#at_"+ordre_combattant[combattant_actuel]).html())-4.0));
                $("#prd_"+ordre_combattant[combattant_actuel]).html(0);
                $("#pi_"+ordre_combattant[combattant_actuel]).html("1D-2");
                $("#arme_"+ordre_combattant[combattant_actuel]).html("Mains nues");
            }
            else
            {
                $("#assaut_"+parseInt(tour_actuel)).append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" devrait casser son arme mais n'en a pas, le tout est donc sans effet (les critiques mains nues ne sont pas encore implementés.)!</p>");
            }
        }
        else if(scoreCritique == 16 || scoreCritique == 17 || scoreCritique == 18)
        {
            //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" reçoit un coup !</p>");
            $("#assaut_"+parseInt(tour_actuel)).append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" reçoit un coup !</p>");
            degats_calc = calcule_degat_ennemi();
            inflige_self_degat(degats_calc);
        }
        else if(scoreCritique == 20)
        {
           //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+", ce looser subit un coup critique avec sa propre arme!</p>");
           $("#assaut_"+parseInt(tour_actuel)).append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+", ce looser subit un coup critique avec sa propre arme!</p>");
           degats_calc = calcule_degat();
           scoreCritique2 = Math.floor(Math.random()*20 + 1);
           degats_calc = subit_attaque_critique(scoreCritique2,degats_calc);
           inflige_self_degat(degats_calc);
        }
    }
    
    function gere_attaque_critique(scoreCritique,degats_calc)
    {
        if(scoreCritique == 1 || scoreCritique == 2)
        {
            degats_calc = degats_calc+1;
            //$("#texte_combat").append("<p>Incision profonde !</p>");        
            $("#assaut_"+parseInt(tour_actuel)).append("<p>Incision profonde !</p>");        
        }
        else if(scoreCritique == 3 || scoreCritique == 4)
        {
            degats_calc = degats_calc+2;
            //$("#texte_combat").append("<p>Incision vraiment profonde !</p>");
            $("#assaut_"+parseInt(tour_actuel)).append("<p>Incision vraiment profonde !</p>");
        }
        else if(scoreCritique == 5 || scoreCritique == 6)
        {
            degats_calc = degats_calc+3;
            //$("#texte_combat").append("<p>Plaie impressionnante et dommage à l'armure !</p>");
            $("#assaut_"+parseInt(tour_actuel)).append("<p>Plaie impressionnante et dommage à l'armure !(-1PR)</p>");
			
			temp = parseInt(parseInt($("#pr_"+ordre_combattant[id_autre_combattant()]).html())-1.0);
			if(temp<0)
			{
				temp = 0;			
			}
			
            $("#pr_"+ordre_combattant[id_autre_combattant()]).html(temp);
        }
        else if(scoreCritique == 7 || scoreCritique == 8)
        {
            degats_calc = degats_calc+4;
            //$("#texte_combat").append("<p>Coup précis et dommage à l'armure !</p>");
            $("#assaut_"+parseInt(tour_actuel)).append("<p>Coup précis et dommage à l'armure (-2PR)!</p>");
            temp = parseInt(parseInt($("#pr_"+ordre_combattant[id_autre_combattant()]).html())-2.0);
			if(temp<0)
			{
				temp = 0;			
			}
			
            $("#pr_"+ordre_combattant[id_autre_combattant()]).html(temp);
        }
        else if(scoreCritique == 10 || scoreCritique == 9)
        {
            degats_calc = degats_calc+5;
            //$("#texte_combat").append("<p>Coup précis de bourrin et dommage à l'armure !</p>");
            $("#assaut_"+parseInt(tour_actuel)).append("<p>Coup précis de bourrin et dommage à l'armure (-3PR)!</p>");
            temp = parseInt(parseInt($("#pr_"+ordre_combattant[id_autre_combattant()]).html())-3.0);
			if(temp<0)
			{
				temp = 0;			
			}
			
            $("#pr_"+ordre_combattant[id_autre_combattant()]).html(temp);
        }
        else if(scoreCritique == 11)
        {
            //$("#texte_combat").append("<p>L'armure est detruite !</p>");
            $("#assaut_"+parseInt(tour_actuel)).append("<p>L'armure est detruite (PR=0)!</p>");
            $("#pr_"+ordre_combattant[id_autre_combattant()]).html(0);
        }
        else if(scoreCritique == 12)
        {
           degats_calc = degats_calc+5;
           $("#at_"+ordre_combattant[id_autre_combattant()]).html(parseInt(parseInt($("#at_"+ordre_combattant[id_autre_combattant()]).html())-1.0));
           $("#prd_"+ordre_combattant[id_autre_combattant()]).html(parseInt(parseInt($("#prd_"+ordre_combattant[id_autre_combattant()]).html())-2.0));
           //$("#texte_combat").append("<p>Un oeil crevé !</p>");
           $("#assaut_"+parseInt(tour_actuel)).append("<p>Un oeil crevé (AT-1, PRD-2)!</p>");
        }
        else if(scoreCritique == 13)
        {
           degats_calc = degats_calc+6;
           $("#at_"+ordre_combattant[id_autre_combattant()]).html(parseInt(parseInt($("#at_"+ordre_combattant[id_autre_combattant()]).html())-5.0));
           $("#prd_"+ordre_combattant[id_autre_combattant()]).html(parseInt(parseInt($("#prd_"+ordre_combattant[id_autre_combattant()]).html())-6.0));
           //$("#texte_combat").append("<p>Une main tranchée !</p>");
           $("#assaut_"+parseInt(tour_actuel)).append("<p>Une main tranchée (AT-5, PRD-6)!</p>");
        }
        else if(scoreCritique == 14)
        {
           degats_calc = degats_calc+6;
           $("#at_"+ordre_combattant[id_autre_combattant()]).html(parseInt(parseInt($("#at_"+ordre_combattant[id_autre_combattant()]).html())-2.0));
           $("#prd_"+ordre_combattant[id_autre_combattant()]).html(parseInt(parseInt($("#prd_"+ordre_combattant[id_autre_combattant()]).html())-2.0));
           //$("#texte_combat").append("<p>Un pied tranché !</p>");
           $("#assaut_"+parseInt(tour_actuel)).append("<p>Un pied tranché (AT-2, PRD-2)!</p>");
        }
        else if(scoreCritique == 15)
        {
           degats_calc = degats_calc+7;
           $("#at_"+ordre_combattant[id_autre_combattant()]).html(parseInt(parseInt($("#at_"+ordre_combattant[id_autre_combattant()]).html())-5.0));
           $("#prd_"+ordre_combattant[id_autre_combattant()]).html(parseInt(parseInt($("#prd_"+ordre_combattant[id_autre_combattant()]).html())-6.0));
           //$("#texte_combat").append("<p>Un bras tranché !</p>");
           $("#assaut_"+parseInt(tour_actuel)).append("<p>Un bras tranché (AT-5, PRD-6)!</p>");
        }
        else if(scoreCritique == 16)
        {
           degats_calc = degats_calc+7;
           $("#at_"+ordre_combattant[id_autre_combattant()]).html(parseInt(parseInt($("#at_"+ordre_combattant[id_autre_combattant()]).html())-5.0));
           $("#prd_"+ordre_combattant[id_autre_combattant()]).html(parseInt(parseInt($("#prd_"+ordre_combattant[id_autre_combattant()]).html())-6.0));
           //$("#texte_combat").append("<p>Une jambe tranchée !</p>");
           $("#assaut_"+parseInt(tour_actuel)).append("<p>Une jambe tranchée (AT-5, PRD-6)!</p>");
        }
        else if(scoreCritique == 17)
        {
           degats_calc = degats_calc+5;
           //$("#texte_combat").append("<p>Organes génitaux endommagés !</p>");
           $("#assaut_"+parseInt(tour_actuel)).append("<p>Organes génitaux endommagés !</p>");
        }
        else if(scoreCritique == 18)
        {
           degats_calc = degats_calc+9;
           //$("#texte_combat").append("<p>Organe vital endommagé !</p>");
           $("#assaut_"+parseInt(tour_actuel)).append("<p>Organe vital endommagé !</p>");
        }
        else if(scoreCritique == 19)
        {
           degats_calc = 200;
           //$("#texte_combat").append("<p>Blessure au coeur !</p>");
           $("#assaut_"+parseInt(tour_actuel)).append("<p>Blessure au coeur !</p>");
        }
        else if(scoreCritique == 20)
        {
           degats_calc = 200;
           //$("#texte_combat").append("<p>Blessure grave à la tete !</p>");
           $("#assaut_"+parseInt(tour_actuel)).append("<p>Blessure grave à la tete !</p>");
        }
        return degats_calc;
    }
    
    function subit_attaque_critique(scoreCritique,degats_calc)
    {
        if(scoreCritique == 1 || scoreCritique == 2)
        {
            degats_calc = degats_calc+1;
            //$("#texte_combat").append("<p>Incision profonde !</p>");        
            $("#assaut_"+parseInt(tour_actuel)).append("<p>Incision profonde !</p>");        
        }
        else if(scoreCritique == 3 || scoreCritique == 4)
        {
            degats_calc = degats_calc+2;
            //$("#texte_combat").append("<p>Incision vraiment profonde !</p>");
            $("#assaut_"+parseInt(tour_actuel)).append("<p>Incision vraiment profonde !</p>");
        }
        else if(scoreCritique == 5 || scoreCritique == 6)
        {
            degats_calc = degats_calc+3;
            //$("#texte_combat").append("<p>Plaie impressionnante et dommage à l'armure !</p>");
            $("#assaut_"+parseInt(tour_actuel)).append("<p>Plaie impressionnante et dommage à l'armure (PR-1)!</p>");
			temp = parseInt(parseInt($("#pr_"+ordre_combattant[combattant_actuel]).html())-1.0);
			if(temp<0)
			{
				temp = 0;			
			}
			
            $("#pr_"+ordre_combattant[combattant_actuel]).html(temp);
        }
        else if(scoreCritique == 7 || scoreCritique == 8)
        {
            degats_calc = degats_calc+4;
            //$("#texte_combat").append("<p>Coup précis et dommage à l'armure !</p>");
            $("#assaut_"+parseInt(tour_actuel)).append("<p>Coup précis et dommage à l'armure (PR-2)!</p>");
            temp = parseInt(parseInt($("#pr_"+ordre_combattant[combattant_actuel]).html())-2.0);
			if(temp<0)
			{
				temp = 0;			
			}
			
            $("#pr_"+ordre_combattant[combattant_actuel]).html(temp);
        }
        else if(scoreCritique == 10 || scoreCritique == 9)
        {
            degats_calc = degats_calc+5;
            //$("#texte_combat").append("<p>Coup précis de bourrin et dommage à l'armure !</p>");
            $("#assaut_"+parseInt(tour_actuel)).append("<p>Coup précis de bourrin et dommage à l'armure (PR-3)!</p>");
            temp = parseInt(parseInt($("#pr_"+ordre_combattant[combattant_actuel]).html())-3.0);
			if(temp<0)
			{
				temp = 0;			
			}
			
            $("#pr_"+ordre_combattant[combattant_actuel]).html(temp);
        }
        else if(scoreCritique == 11)
        {
            //$("#texte_combat").append("<p>L'armure est detruite !</p>");
            $("#assaut_"+parseInt(tour_actuel)).append("<p>L'armure est detruite !</p>");
            $("#pr_"+ordre_combattant[combattant_actuel]).html(0);
        }
        else if(scoreCritique == 12)
        {
           degats_calc = degats_calc+5;
           $("#at_"+ordre_combattant[combattant_actuel]).html(parseInt(parseInt($("#at_"+ordre_combattant[combattant_actuel]).html())-1.0));
           $("#prd_"+ordre_combattant[combattant_actuel]).html(parseInt(parseInt($("#prd_"+ordre_combattant[combattant_actuel]).html())-2.0));
           //$("#texte_combat").append("<p>Un oeil crevé !</p>");
           $("#assaut_"+parseInt(tour_actuel)).append("<p>Un oeil crevé (AT-1, PRD-2)!</p>");
        }
        else if(scoreCritique == 13)
        {
           degats_calc = degats_calc+6;
           $("#at_"+ordre_combattant[combattant_actuel]).html(parseInt(parseInt($("#at_"+ordre_combattant[combattant_actuel]).html())-5.0));
           $("#prd_"+ordre_combattant[combattant_actuel]).html(parseInt(parseInt($("#prd_"+ordre_combattant[combattant_actuel]).html())-6.0));
           //$("#texte_combat").append("<p>Une main tranchée !</p>");
           $("#assaut_"+parseInt(tour_actuel)).append("<p>Une main tranchée (AT-5, PRD-6)!</p>");
        }
        else if(scoreCritique == 14)
        {
           degats_calc = degats_calc+6;
           $("#at_"+ordre_combattant[combattant_actuel]).html(parseInt(parseInt($("#at_"+ordre_combattant[combattant_actuel]).html())-2.0));
           $("#prd_"+ordre_combattant[combattant_actuel]).html(parseInt(parseInt($("#prd_"+ordre_combattant[combattant_actuel]).html())-2.0));
           //$("#texte_combat").append("<p>Un pied tranché !</p>");
           $("#assaut_"+parseInt(tour_actuel)).append("<p>Un pied tranché (AT-2, PRD-2)!</p>");
        }
        else if(scoreCritique == 15)
        {
           degats_calc = degats_calc+7;
           $("#at_"+ordre_combattant[combattant_actuel]).html(parseInt(parseInt($("#at_"+ordre_combattant[combattant_actuel]).html())-5.0));
           $("#prd_"+ordre_combattant[combattant_actuel]).html(parseInt(parseInt($("#prd_"+ordre_combattant[combattant_actuel]).html())-6.0));
           //$("#texte_combat").append("<p>Un bras tranché !</p>");
           $("#assaut_"+parseInt(tour_actuel)).append("<p>Un bras tranché (AT-5, PRD-6)!</p>");
        }
        else if(scoreCritique == 16)
        {
           degats_calc = degats_calc+7;
           $("#at_"+ordre_combattant[combattant_actuel]).html(parseInt(parseInt($("#at_"+ordre_combattant[combattant_actuel]).html())-5.0));
           $("#prd_"+ordre_combattant[combattant_actuel]).html(parseInt(parseInt($("#prd_"+ordre_combattant[combattant_actuel]).html())-6.0));
           //$("#texte_combat").append("<p>Une jambe tranchée !</p>");
           $("#assaut_"+parseInt(tour_actuel)).append("<p>Une jambe tranchée (AT-5, PRD-6)!</p>");
        }
        else if(scoreCritique == 17)
        {
           degats_calc = degats_calc+5;
           //$("#texte_combat").append("<p>Organes génitaux endommagés !</p>");
           $("#assaut_"+parseInt(tour_actuel)).append("<p>Organes génitaux endommagés !</p>");
        }
        else if(scoreCritique == 18)
        {
           degats_calc = degats_calc+9;
           //$("#texte_combat").append("<p>Organe vital endommagé !</p>");
           $("#assaut_"+parseInt(tour_actuel)).append("<p>Organe vital endommagé !</p>");
        }
        else if(scoreCritique == 19)
        {
           degats_calc = 200;
           //$("#texte_combat").append("<p>Blessure au coeur !</p>");
           $("#assaut_"+parseInt(tour_actuel)).append("<p>Blessure au coeur !</p>");
        }
        else if(scoreCritique == 20)
        {
           degats_calc = 200;
           //$("#texte_combat").append("<p>Blessure grave à la tete !</p>");
           $("#assaut_"+parseInt(tour_actuel)).append("<p>Blessure grave à la tete !</p>");
        }
        return degats_calc;
    }
    
    function gere_parade_maladresse(scoreCritique,degats_calc)
    {
        if(scoreCritique == 1 || scoreCritique == 2)
        {
            $("#assaut_"+parseInt(tour_actuel)).append("<p>Rattrapage in extremis !</p>"); 
            degat_calcule = calcule_degat();
            inflige_degat(degat_calcule,0);
        }
        else if(scoreCritique == 3 || scoreCritique == 4 || scoreCritique == 5)
        {
            aventurierAfk[id_autre_combattant()] = 2;
            $("#assaut_"+parseInt(tour_actuel)).append("<p>Trebuche et chute maladroitement !</p>");
            degat_calcule = calcule_degat();
            inflige_degat(degat_calcule,0);
        }
        else if(scoreCritique == 6 || scoreCritique == 7)
        {
            $("#assaut_"+parseInt(tour_actuel)).append("<p>Frappe un allié à gauche (en developpement) !</p>");
            degat_calcule = calcule_degat();
            inflige_degat(degat_calcule,0);
        }
        else if(scoreCritique == 8 || scoreCritique == 9)
        {
            $("#assaut_"+parseInt(tour_actuel)).append("<p>Frappe un allié à droite (en developpement) !</p>");
            degat_calcule = calcule_degat();
            inflige_degat(degat_calcule,0);
        }
        else if(scoreCritique == 10 || scoreCritique == 11 || scoreCritique == 12)
        {
            if($("#arme_"+ordre_combattant[id_autre_combattant()]).html() != "Mains nues")
            {
                degats_calc = degats_calc+5;
                $("#assaut_"+parseInt(tour_actuel)).append("<p>Lache son arme (AT-4, PRD=0, PI=1D-2)!</p>");
                $("#at_"+ordre_combattant[id_autre_combattant()]).html(parseInt(parseInt($("#at_"+ordre_combattant[id_autre_combattant()]).html())-4.0));
                $("#prd_"+ordre_combattant[id_autre_combattant()]).html(0);
                $("#pi_"+ordre_combattant[id_autre_combattant()]).html("1D-2");
                $("#arme_"+ordre_combattant[id_autre_combattant()]).html("Mains nues");
                degat_calcule = calcule_degat();
                inflige_degat(degat_calcule,0);
            }
            else
            {
                $("#assaut_"+parseInt(tour_actuel)).append("<p>Devrait lacher son arme mais n'en a pas (les critiques mains nues ne sont pas encore implementés.)!</p>");
                
            }
        }
        else if(scoreCritique == 13 || scoreCritique == 14 || scoreCritique == 15)
        {
            if($("#arme_"+ordre_combattant[id_autre_combattant()]).html() != "Mains nues")
            {
                $("#assaut_"+parseInt(tour_actuel)).append("<p>Casse son arme (AT-4, PRD=0, PI=1D-2)!</p>");
                $("#at_"+ordre_combattant[id_autre_combattant()]).html(parseInt(parseInt($("#at_"+ordre_combattant[id_autre_combattant()]).html())-4.0));
                $("#prd_"+ordre_combattant[id_autre_combattant()]).html(0);
                $("#pi_"+ordre_combattant[id_autre_combattant()]).html("1D-2");
                $("#arme_"+ordre_combattant[id_autre_combattant()]).html("Mains nues");
                degat_calcule = calcule_degat();
                inflige_degat(degat_calcule,0);
            }
            else
            {
                $("#assaut_"+parseInt(tour_actuel)).append("<p>Devrait lacher son arme mais n'en a pas (les critiques mains nues ne sont pas encore implementés.)!</p>");                
            }
        }
        else if(scoreCritique == 16 || scoreCritique == 17 || scoreCritique == 18)
        {
           $("#assaut_"+parseInt(tour_actuel)).append("<p>Se blesse tout seul comme un cake  !</p>");
           inflige_degat(calcule_degat_ennemi(),0);    
            degat_calcule = calcule_degat();
            inflige_degat(degat_calcule,0);
        }
        else if(scoreCritique == 19)
        {
            $("#assaut_"+parseInt(tour_actuel)).append("<p>Se blesse très sévèrement de façon atroce et douloureuse  !</p>");
            inflige_degat(parseInt(calcule_degat_ennemi()+6),0);  
            degat_calcule = calcule_degat();
            inflige_degat(degat_calcule,0);
        }
        else if(scoreCritique == 20)
        {
           $("#assaut_"+parseInt(tour_actuel)).append("<p>Se crève un oeil (AT-1, PRD-2)!</p>");
           inflige_degat(parseInt(calcule_degat_ennemi()+5),0);      
           $("#at_"+ordre_combattant[id_autre_combattant()]).html(parseInt(parseInt($("#at_"+ordre_combattant[id_autre_combattant()]).html())-1.0));
           $("#prd_"+ordre_combattant[id_autre_combattant()]).html(parseInt(parseInt($("#prd_"+ordre_combattant[id_autre_combattant()]).html())-2.0));
           degat_calcule = calcule_degat();
           inflige_degat(degat_calcule,0);
        }
    }
    
    function gere_maladresse_critique(scoreCritique,degats_calc)
    {
        if(scoreCritique == 1 || scoreCritique == 2)
        {
            $("#assaut_"+parseInt(tour_actuel)).append("<p>Rattrapage in extremis !</p>");        
        }
        else if(scoreCritique == 3 || scoreCritique == 4 || scoreCritique == 5)
        {
            aventurierAfk[combattant_actuel] = 2;
            $("#assaut_"+parseInt(tour_actuel)).append("<p>Trebuche et chute maladroitement !</p>");
        }
        else if(scoreCritique == 6 || scoreCritique == 7)
        {
            $("#assaut_"+parseInt(tour_actuel)).append("<p>Frappe un allié à gauche (en developpement) !</p>");
        }
        else if(scoreCritique == 8 || scoreCritique == 9)
        {
            $("#assaut_"+parseInt(tour_actuel)).append("<p>Frappe un allié à droite (en developpement) !</p>");
        }
        else if(scoreCritique == 10 || scoreCritique == 11 || scoreCritique == 12)
        {
            if($("#arme_"+ordre_combattant[combattant_actuel]).html() != "Mains nues")
            {
                degats_calc = degats_calc+5;                
                $("#assaut_"+parseInt(tour_actuel)).append("<p>Lache son arme (AT-4, PRD=0, PI=1D-2)!</p>");
                $("#at_"+ordre_combattant[combattant_actuel]).html(parseInt(parseInt($("#at_"+ordre_combattant[combattant_actuel]).html())-4.0));
                $("#prd_"+ordre_combattant[combattant_actuel]).html(0);
                $("#pi_"+ordre_combattant[combattant_actuel]).html("1D-2");
                $("#arme_"+ordre_combattant[combattant_actuel]).html("Mains nues");
            }
            else
            {
                $("#assaut_"+parseInt(tour_actuel)).append("<p>Devrait lacher son arme, mais n'en a pas (les critiques mains nues ne sont pas encore implementés.)!</p>");
            }
        }
        else if(scoreCritique == 13 || scoreCritique == 14 || scoreCritique == 15)
        {
            if($("#arme_"+ordre_combattant[combattant_actuel]).html() != "Mains nues")
            {
                $("#assaut_"+parseInt(tour_actuel)).append("<p>Casse son arme (AT-4, PRD=0, PI=1D-2)!</p>");
                $("#at_"+ordre_combattant[combattant_actuel]).html(parseInt(parseInt($("#at_"+ordre_combattant[combattant_actuel]).html())-4.0));
                $("#prd_"+ordre_combattant[combattant_actuel]).html(0);
                $("#pi_"+ordre_combattant[combattant_actuel]).html("1D-2");
                $("#arme_"+ordre_combattant[combattant_actuel]).html("Mains nues");
            }
            else
            {
                $("#assaut_"+parseInt(tour_actuel)).append("<p>Devrait casser son arme, mais n'en a pas (les critiques mains nues ne sont pas encore implementés.)!</p>");
            }
        }
        else if(scoreCritique == 16 || scoreCritique == 17 || scoreCritique == 18)
        {
           $("#assaut_"+parseInt(tour_actuel)).append("<p>Se blesse tout seul comme un cake  !</p>");
           degats_calc = degats_calc+5;
           inflige_self_degat(calcule_degat());
           
        }
        else if(scoreCritique == 19)
        {
           $("#assaut_"+parseInt(tour_actuel)).append("<p>Se blesse très sévèrement de façon atroce et douloureuse  !</p>");
           degats_calc = degats_calc+6;
           inflige_self_degat(degats_calc);           
        }
        else if(scoreCritique == 20)
        {
           $("#assaut_"+parseInt(tour_actuel)).append("<p>Se crève un oeil (AT-1, PRD-2)!</p>");
           degats_calc = degats_calc+5;
           inflige_self_degat(degats_calc);
           $("#at_"+ordre_combattant[combattant_actuel]).html(parseInt(parseInt($("#at_"+ordre_combattant[combattant_actuel]).html())-1.0));
           $("#prd_"+ordre_combattant[combattant_actuel]).html(parseInt(parseInt($("#prd_"+ordre_combattant[combattant_actuel]).html())-2.0));
        }
    }
    
    function calcule_degat()
    {
        degat_pi = $("#pi_"+ordre_combattant[combattant_actuel]).html();
        index1 = degat_pi.indexOf("D");
        nbrDes = degat_pi.substring(0,index1);
        nbrDegat1 = 0;
        nbrDegat2 = 0;
        reste = degat_pi.substring(index1);
        
        index2 = reste.indexOf("+");
        if(index2 != -1)
        {
            nbrDegat1 = reste.substring(index2+1,index2+2);
            reste = reste.substring(index2+2);
            index3 = reste.indexOf("+");
            if(index3 != -1)
            {
                nbrDegat2 = reste.substring(index3+1,index3+2);
            }
            else
            {
                index3 = reste.indexOf("-");
                if(index3 != -1)
                {
                    nbrDegat2 = -1*parseInt(reste.substring(index3+1,index3+2));
                }
            }
        }
        else
        {
            index2 = reste.indexOf("-");
            if(index2 != -1)
            {
                nbrDegat1 = -1*parseInt(reste.substring(index2+1,index2+2));
                reste = reste.substring(index2+2);
                index3 = reste.indexOf("+");
                if(index3 != -1)
                {
                    nbrDegat2 = reste.substring(index3+1,index3+2);
                }
                else
                {
                    index3 = reste.indexOf("-");
                    if(index3 != -1)
                    {
                        nbrDegat2 = -1*parseInt(reste.substring(index3+1,index3+2));
                    }
                }
            }
        }                        
        
        degat = 0;
        for(a=0; a<nbrDes; a++)
        {
            degat = degat + Math.floor(Math.random()*6 + 1);
        }
        
        degat = parseInt(degat) + parseInt(nbrDegat1) + parseInt(nbrDegat2);
        return degat;
    }
    
    function calcule_degat_ennemi()
    {
        degat_pi = $("#pi_"+ordre_combattant[id_autre_combattant()]).html();
        index1 = degat_pi.indexOf("D");
        nbrDes = degat_pi.substring(0,index1);
        nbrDegat1 = 0;
        nbrDegat2 = 0;
        reste = degat_pi.substring(index1);
        
        index2 = reste.indexOf("+");
        if(index2 != -1)
        {
            nbrDegat1 = reste.substring(index2+1,index2+2);
            reste = reste.substring(index2+2);
            index3 = reste.indexOf("+");
            if(index3 != -1)
            {
                nbrDegat2 = reste.substring(index3+1,index3+2);
            }
            else
            {
                index3 = reste.indexOf("-");
                if(index3 != -1)
                {
                    nbrDegat2 = -1*parseInt(reste.substring(index3+1,index3+2));
                }
            }
        }
        else
        {
            index2 = reste.indexOf("-");
            if(index2 != -1)
            {
                nbrDegat1 = -1*parseInt(reste.substring(index2+1,index2+2));
                reste = reste.substring(index2+2);
                index3 = reste.indexOf("+");
                if(index3 != -1)
                {
                    nbrDegat2 = reste.substring(index3+1,index3+2);
                }
                else
                {
                    index3 = reste.indexOf("-");
                    if(index3 != -1)
                    {
                        nbrDegat2 = -1*parseInt(reste.substring(index3+1,index3+2));
                    }
                }
            }
        }                        
        
        degat = 0;
        for(a=0; a<nbrDes; a++)
        {
            degat = degat + Math.floor(Math.random()*6 + 1);
        }
        
        degat = parseInt(degat) + parseInt(nbrDegat1) + parseInt(nbrDegat2);
        return degat;
    }
    
    function inflige_degat(degat__,crit)
    {
        //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" fait "+degat__+" point de dégats.</p>");                
        $("#assaut_"+parseInt(tour_actuel)).append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" fait <b>"+degat__+"</b> point de dégats.</p>");                
        degat_reel = 0;
        if(crit == 1)
        {
            //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[id_autre_combattant()]).html()+" reçoit "+degat__+" points de dégats.</p>");
            $("#assaut_"+parseInt(tour_actuel)).append("<p>"+$("#nom_"+ordre_combattant[id_autre_combattant()]).html()+" reçoit <b>"+degat__+"</b> points de dégats.</p>");
            degat_reel = parseInt(degat__);
        
        }
        else
        {
            degat_reel = parseInt(degat__)-parseInt($("#pr_"+ordre_combattant[id_autre_combattant()]).html());
            if(degat_reel < 0)
            {
                degat_reel = 0;
            }
            //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[id_autre_combattant()]).html()+" reçoit "+degat__+"-"+$("#pr_"+ordre_combattant[id_autre_combattant()]).html()+"(PR) = "+degat_reel+" points de dégats.</p>");
            $("#assaut_"+parseInt(tour_actuel)).append("<p>"+$("#nom_"+ordre_combattant[id_autre_combattant()]).html()+" reçoit "+degat__+"-"+$("#pr_"+ordre_combattant[id_autre_combattant()]).html()+"(PR) = <b>"+degat_reel+"</b> points de dégats.</p>");
        }
        degat_reel = parseInt(degat__)-parseInt($("#pr_"+ordre_combattant[id_autre_combattant()]).html());
        if(degat_reel < 0)
        {
            degat_reel = 0;
        }
        
        $("#ev_"+ordre_combattant[id_autre_combattant()]).html(parseInt(parseInt($("#ev_"+ordre_combattant[id_autre_combattant()]).html()) - parseInt(degat_reel)));
        if(parseInt($("#ev_"+ordre_combattant[id_autre_combattant()]).html()) > 2)
        {
            //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[id_autre_combattant()]).html()+" a encore "+$("#ev_"+ordre_combattant[id_autre_combattant()]).html()+" points de vie.</p>");                
            $("#assaut_"+parseInt(tour_actuel)).append("<p>"+$("#nom_"+ordre_combattant[id_autre_combattant()]).html()+" a encore "+$("#ev_"+ordre_combattant[id_autre_combattant()]).html()+" points de vie.</p>");                
        }
        else if(parseInt($("#ev_"+ordre_combattant[id_autre_combattant()]).html()) > 0)
        {
            //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[id_autre_combattant()]).html()+" n'a plus que "+$("#ev_"+ordre_combattant[id_autre_combattant()]).html()+" points de vie et tombe dans les pommes.</p>");                
            $("#assaut_"+parseInt(tour_actuel)).append("<p>"+$("#nom_"+ordre_combattant[id_autre_combattant()]).html()+" n'a plus que "+$("#ev_"+ordre_combattant[id_autre_combattant()]).html()+" points de vie et tombe dans les pommes.</p>");                
            $("#ev_"+ordre_combattant[id_autre_combattant()]).html("Inconscient");     
        }
        else
        {
            $("#ev_"+ordre_combattant[id_autre_combattant()]).html("Mort");                    
            //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[id_autre_combattant()]).html()+" meurt suite à cette attaque.</p>");                
           $("#assaut_"+parseInt(tour_actuel)).append("<p>"+$("#nom_"+ordre_combattant[id_autre_combattant()]).html()+" meurt suite à cette attaque.</p>");                
        }
    }
    
    function inflige_self_degat(degat__)
    {
        //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" se fait "+degat__+" point de dégats.</p>");                
        $("#assaut_"+parseInt(tour_actuel)).append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" se fait "+degat__+" point de dégats.</p>");                
        //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" reçoit "+degat__+"-"+$("#pr_"+ordre_combattant[combattant_actuel]).html()+"(PR) points de dégats.</p>");
        
        degat_reel = parseInt(degat__)-+parseInt($("#pr_"+ordre_combattant[combattant_actuel]).html());
        if(degat_reel < 0)
        {
            degat_reel = 0;
        }
        
        $("#assaut_"+parseInt(tour_actuel)).append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" reçoit "+degat__+"-"+$("#pr_"+ordre_combattant[combattant_actuel]).html()+"(PR) = "+degat_reel+" points de dégats.</p>");
       
        
        
        $("#ev_"+ordre_combattant[combattant_actuel]).html(parseInt(parseInt($("#ev_"+ordre_combattant[combattant_actuel]).html()) - parseInt(degat_reel)));
        if(parseInt($("#ev_"+ordre_combattant[combattant_actuel]).html()) > 2)
        {
            //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" a encore "+$("#ev_"+ordre_combattant[combattant_actuel]).html()+" points de vie.</p>");                
            $("#assaut_"+parseInt(tour_actuel)).append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" a encore "+$("#ev_"+ordre_combattant[combattant_actuel]).html()+" points de vie.</p>");                
        }
        else if(parseInt($("#ev_"+ordre_combattant[combattant_actuel]).html()) > 0)
        {
            //$("#texte_combat").append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" n'a plus que "+$("#ev_"+ordre_combattant[combattant_actuel]).html()+" points de vie et tombe dans les pommes.</p>");                
            $("#assaut_"+parseInt(tour_actuel)).append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" n'a plus que "+$("#ev_"+ordre_combattant[combattant_actuel]).html()+" points de vie et tombe dans les pommes.</p>");                
            $("#ev_"+ordre_combattant[combattant_actuel]).html("Inconscient");     
        }
        else
        {
            $("#ev_"+ordre_combattant[combattant_actuel]).html("Mort");                    
            $("#assaut_"+parseInt(tour_actuel)).append("<p>"+$("#nom_"+ordre_combattant[combattant_actuel]).html()+" meurt suite à cette attaque.</p>");                
        }
    }
    
    function id_autre_combattant()
    {
        temp = combattant_actuel;
        temp++;
        if(temp >= ordre_combattant.length)
        {
            temp = 0;
        }
        return temp;
    }

</script>
