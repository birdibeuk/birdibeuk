<div class='principal_avec_pub'>
    Sélectionnez l'aventurier :<br>
    <p  style='text-align:center'>
        <form action='index.php' style='text-align:center'>
            Je cherche un aventurier dont le nom s'écrit ou commence par &nbsp;&nbsp;
            <input type='text' name='nom' value='<?php if(isset($_GET["nom"])){echo $_GET["nom"];}?>' />
            <input type='hidden' name='ctrl' value='outilsMJ' />
            <input type='hidden' name='action' value='simulation_combat' />
            <input type='submit' value='Chercher' />
        </form>
    </p>
    <form action='index.php?ctrl=outilsMJ&action=simulation_combat_mobs_selection' method='post'>
        <?php 
            if(count($mesAventuriers) > 0)
            {
                ?>
                <table style='margin:auto;'>
                    <tr>
                        <th colspan='5'><b><u>Vos aventuriers</u></b><br></th>                
                    </tr>
                    <tr>
                        <th style='width:10px;text-align:left;'></th>
                        <th style='width:200px;text-align:left;'><u>Nom</u></th>           
                        <th style='width:100px;text-align:left;'><u>Origine</u></th>
                        <th style='width:100px;text-align:left;'><u>Métier</u></th>
                        <th style='width:50px;text-align:left;'><u>Niveau</u></th>                
                    </tr>
                    <?php 
                        $maxID = 0;
                        foreach($mesAventuriers as $aventurier)
                        {
                            echo "<tr>";
                            echo "  <td><input type='radio' name='aventurier' value='".$aventurier->ID."' /></td>
                                    <td>".$aventurier->NOM."</td>
                                    <td>".$aventurier->ORIGINE."</td>
                                    <td>".$aventurier->METIER."</td>
                                    <td>".$aventurier->NIVEAU."</td>  ";                          
                            echo "</tr>";
                            if($aventurier->ID > $maxID)
                            {
                                $maxID = $aventurier->ID;
                            }
                        }
                    ?>
                </table>
                <?php
            }
        ?>
        
        <br><br>
        <table style='margin:auto;'>
            <tr>
                <th style='width:10px;text-align:left;'></th>
                <th style='width:200px;text-align:left;'><u>Nom</u></th>           
                <th style='width:100px;text-align:left;'><u>Origine</u></th>
                <th style='width:100px;text-align:left;'><u>Métier</u></th>
                <th style='width:50px;text-align:left;'><u>Niveau</u></th>                
            </tr>
            <div style='text-align:center;'><input type='submit' value='Continuer et selectionner un ennemi' style='width:400px;'/></div>
            <?php 
                $maxID = 0;
                foreach($aventuriers as $aventurier)
                {
                    echo "<tr>";
                    echo "  <td><input type='radio' name='aventurier' value='".$aventurier->ID."' /></td>
                            <td>".$aventurier->NOM."</td>
                            <td>".$aventurier->ORIGINE."</td>
                            <td>".$aventurier->METIER."</td>
                            <td>".$aventurier->NIVEAU."</td>  ";                          
                    echo "</tr>";
                    if($aventurier->ID > $maxID)
                    {
                        $maxID = $aventurier->ID;
                    }
                }
            ?>
        </table>
        <input type='hidden' value='<?php echo $maxID; ?>' name='maxID' />
        <div style='text-align:center;'><input type='submit' value='Continuer et selectionner un monstre ennemi' style='width:400px;'/></div>
    </form>
</div>
