<?php 
session_start();
header ("Content-type: image/png");
$image = imagecreatefrompng(__DIR__."/../image/carte_mob.png");

include(__DIR__."/../config/param.php");
include(__DIR__."/../model/GeneralFunctions.php");
include(__DIR__."/../model/autoloader.php");

$mob;
if(isset($_SESSION["current_mob".$_GET["str"]]))
{
    $mob = unserialize($_SESSION["current_mob".$_GET["str"]]);
    unset($_SESSION["current_mob".$_GET["str"]]);
}
else
{
    $mob = new Mob(17);
    $mob->getLoot();
}    

$noir = imagecolorallocate($image, 0, 0, 0);
$vert = imagecolorallocate($image, 0, 144, 0);
$rouge = imagecolorallocate($image, 144, 0, 0);

$font = __DIR__."/../fonts/VINERITC.TTF"; 
if(isset($_GET["font"]))
{
	$font = __DIR__."/../fonts/".$_GET["font"].".TTF"; 
}

$fontSize = 30;
$fontSizeEquipement = 30;
$font_size_temp = 30;

$dimensions = imagettfbbox($fontSize, 0, $font, $mob->nom);
$lineWidth = $dimensions[2] - $dimensions[0];
while(($lineWidth = ($dimensions[2] - $dimensions[0])) > 250)
{
    $font_size_temp = $font_size_temp - 1;
    $dimensions = imagettfbbox($font_size_temp, 0, $font, $mob->nom);
    $lineWidth = $dimensions[2] - $dimensions[0];
}    
$position = 206 - ($lineWidth/2);


imagettftext($image, $font_size_temp, 0, $position, 40, $noir, $font, $mob->nom);
imagettftext($image, $font_size_temp, 0, 290, 100, $noir, $font, $mob->at);
imagettftext($image, $font_size_temp, 0, 340, 100, $noir, $font, $mob->prd);
imagettftext($image, 15, 0, 150, 160, $noir, $font, $mob->pi);
imagettftext($image, 18, 0, 150, 235, $noir, $font, $mob->pr);
imagettftext($image, 18, 0, 195, 310, $noir, $font, $mob->cou);
imagettftext($image, 18, 0, 130, 370, $noir, $font, $mob->xp);
imagettftext($image, 18, 0, 305, 370, $noir, $font, $mob->loot["po"]."PO");
imagettftext($image, 18, 0, 255, 445, $noir, $font, $mob->ev);

$fontSize = 18;
$fontSizeEquipement = 18;
$font_size_temp = 18;

$nbr_equ = 0;

//arme
foreach($mob->loot as $arme)
{
    if(is_a($arme, 'Arme'))
    {
        $string = $arme->NOM;        
        $dimensions = imagettfbbox($fontSize, 0, $font, $string);
        $lineWidth = $dimensions[2] - $dimensions[0];
        while(($lineWidth = ($dimensions[2] - $dimensions[0])) > 350)
        {
            $font_size_temp = $font_size_temp - 1;
            $dimensions = imagettfbbox($font_size_temp, 0, $font, $string);
            $lineWidth = $dimensions[2] - $dimensions[0];
        }    
        $position = 206 - ($lineWidth/2);

        imagettftext($image, $font_size_temp, 0, $position, 590+(30*$nbr_equ), $noir, $font, $string);
        $nbr_equ++;
    }
}
 

foreach($mob->loot as $key=>$truc)
{
    if(is_a($truc, 'Equipement'))
    {       
        $string = "";
        
        if($truc->nombre > 1)
        {
            $string .= $truc->nombre." ";
        }
        $string .= $truc->NOM." (".$truc->PO.",".$truc->PA.$truc->PC.")";
        $font_size_temp = $fontSize;
        $dimensions = imagettfbbox($fontSize, 0, $font, $string);
        $lineWidth = $dimensions[2] - $dimensions[0];
        while(($lineWidth = ($dimensions[2] - $dimensions[0])) > 350)
        {
            $font_size_temp = $font_size_temp - 1;
            $dimensions = imagettfbbox($font_size_temp, 0, $font, $string);
            $lineWidth = $dimensions[2] - $dimensions[0];
        }    
        $position = 206 - ($lineWidth/2);
        
        imagettftext($image, $font_size_temp, 0, $position, 590+(30*$nbr_equ), $noir, $font, $string);
        $nbr_equ++;
    }
}

if(isset($_GET["w"]) && isset($_GET["h"]))
{
    $image = imagescale($image,$_GET["w"],$_GET["h"]);
}

imagepng($image); 
?>