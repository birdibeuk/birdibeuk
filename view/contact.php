<div class='principal_avec_pub'>
	<p style='text-align:center;'>
		Vous avez détecté une erreur ? <br>Vous avez des idées, des suggestions ?<br>Vous voulez partager votre recette de pancréas de globzoule Alpha aux framboises ?
		<br><br>Ne vous génez pas !
	</p>
	<form id='form' method='post' action='index.php?ctrl=contact'>
		Votre nom : <input type='text' name='nom' id='nom' style='width:500px;' /><br>
        <br>
		Votre email : <input type='text' name='email' id='email' style='width:500px;' /><br>
        <br>
		Votre message : 
        <textarea name='message' id='message' style='width:97%;height:300px;' class='contact'></textarea><br>
		<br>
		<div style='text-align:center;'>
            <input class='bouton' type='button' onclick='check_form();' value='Envoyer par mail à l&apos;administrateur' style='width:500px;' />
        </div>
	</form>
</div>
<script>
    function check_form()
    {
        valid = true;
        
        if(document.getElementById('nom').value == "")
		{
			valid = false;
			alert("Le nom ne peut être vide.");
		}
        
        if(valid && document.getElementById('email').value == "")
        {				
            valid = false;
            alert("L'email est obligatoire.");
        }
        else
        {
            if(valid && !isValidEmailAddress(document.getElementById('email').value) )
            {
                valid = false;
                alert("L'email est invalide.");
            }
        }
        
        if(valid && document.getElementById('message').value == "")
        {				
            valid = false;
            alert("Le message ne peut être vide.");
        }
        
        if(valid)
        {
            $("#form").submit();
        }
    }
</script>