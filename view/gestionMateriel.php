<div class='principal_avec_pub'>
	<h1 style='text-align:center;'>Le matériel que vous avez créé</h1>
        <table class='default'>
        <tr><th colspan='8'><b>Armes</b></th></tr>
        <tr>
            <th>Nom</th><th>Type</th><th>Prix</th><th>PI</th>
            <th>Rupture</th><th>Modifications</th><th>Nbr Possesseurs</th><th>Actions</th>
        </tr>
        <?php
            $max_id = 0;
            foreach($armes as $arme)
            {
                if($arme->ID > $max_id)
                {
                    $max_id = $arme->ID;
                }

                echo "<tr id='TR_ARME_".$arme->ID."'>";
                    echo "<td>";
                    if($arme->officiel == 1){echo "<u>";}
                    echo $arme->NOM;
                    if($arme->officiel == 1){echo "</u>";}
                    echo "</td>";
                    echo "<td>".$arme->type."</td>";
                    echo "<td>".$arme->PRIX."</td>";
                    echo "<td>".$arme->PI."</td>";
                    echo "<td>".$arme->RUP."</td>";
                    echo "<td>".$arme->modif()."</td>";
                    echo "<td>".$arme->compte."</td>";
                    if($arme->debase)
                    {
                        echo "<!--debase-->";
                    }
                    echo "<td>";
                    if($arme->officiel != 1)
                    {
                        echo "<a href='index.php?ctrl=gestionMateriel&action=modifierArme&id=".$arme->ID."'><span style='cursor:pointer;' onclick='modifyArme(".$arme->ID.");' class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span></a> ";
                        echo " <span style='cursor:pointer;' onclick='deleteArme(".$arme->ID.");' class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span>";
                    }
                    echo "</td>";
                echo "<tr>";
            }
        ?>
    </table>
    <br>
        <br>
        <div id='nbre_equipement' style='text-align:center;'></div>
        <table class='default'>
        <tr>
            <th colspan='6'>Protections</th>
        </tr>
        <tr>
           <th>Nom</th><th>Prix</th><th>PR</th>
            <th>Rupture</th><th>Modifications</th><th>Actions</th>
        </tr>
        <?php
            $max_id = 0;
            foreach($protections as $protection)
            {
                if($protection->ID > $max_id)
                {
                    $max_id = $protection->ID;
                }

                echo "<tr id='TR_PROTECTION_".$protection->ID."'>";
                    echo "<td>".$protection->NOM."</td>";
                    echo "<td>".$protection->PRIX."</td>";
                    echo "<td>".$protection->PR."</td>";
                    echo "<td>".$protection->RUP."</td>";
                    echo "<td>".$protection->modif()."</td><!--".$protection->TYPE."-->";
                    if($protection->debase)
                    {
                        echo "<!--debase-->";
                    }
                    echo "<td> </td>";
                echo "<tr>";
            }
        ?>
    </table>
    <br>
    <br>
    <div id='nbre_equipement' style='text-align:center;'></div>
    <table class='default'>
        <tr>
            <th colspan='6'>Equipements</th>
        </tr>
        <tr>
            <th>Nom</th><th>PO</th><th>PA</th><th>PC</th><th>Special</th><th>Action</th>
        </tr>
    <?php
        $max_id = 0;
        foreach($equipements as $equipement)
        {
            if($equipement->ID > $max_id)
            {
                $max_id = $equipement->ID;
            }

            echo "<tr id='TR_EQUI_".$equipement->ID."'>";
                echo "<td>".$equipement->NOM."</td>";
                echo "<td>".$equipement->PO."</td>";
                echo "<td>".$equipement->PA."</td>";
                echo "<td>".$equipement->PC."</td>";
                echo "<td>".$equipement->SPECIAL."</td><!--".$equipement->type."-->";
                if($equipement->debase)
                {
                    echo "<!--debase-->";
                }
                //echo "<td> <a href='index.php?ctrl=userEquipements&action=modifier&id=".$equipement->ID."'><img style='width:20px;height:20px;margin-bottom:5px;' src='image/pencil.png'/></a> <a href='index.php?ctrl=userEquipements&action=supprimer&id=".$equipement->ID."'><img style='width:30px;height:30px;' src='image/delete.png'/></a></td>";
            echo "<tr>";
        }
    ?>
    </table>
</div>

<script>
    function deleteArme(id_arme)
    {
      //test
        $.ajax({
				method: "POST",
				url: "ajax/deleteArme.php",
				data: { "id_arme" : id_arme }
			}).done(function(result)
				{
					if(result == 1)
                    {
                        alert("Arme supprimée.");
                        $("#TR_ARME_"+id_arme).hide();
                    }
                    else
                    {
                        alert("Une erreur est survenue.");
                    }
				});

    }
</script>