<div class='principal_avec_pub'>
    <style>
        #map_canvas 
        {
            height: 700px;
            width: 900px;
            margin:50px auto;
        }
    </style>
	<div id="map_canvas" style='border: 3px #900000 solid;-webkit-border-radius: 20px;
    -moz-border-radius: 20px;
    border-radius: 20px;'></div>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script> 
    <script>               
        var geocoder =  new google.maps.Geocoder();
        
		var infowindow = new google.maps.InfoWindow();
		var latlng = new google.maps.LatLng(46.52359,2.33288);
		var mapOptions = {
                zoom: 6,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			
		var geo = new google.maps.Geocoder(); 
		var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
		var bounds = new google.maps.LatLngBounds();
		
		function createMarker(add,lat,lng,txt) {
		   var contentString = add;
		   var marker = new google.maps.Marker({
			 position: new google.maps.LatLng(lat,lng),
			 map: map,
			 title: txt,
			 zIndex: Math.round(latlng.lat()*-100000)<<5
		   });

		  google.maps.event.addListener(marker, 'click', function() {
			 infowindow.setContent(contentString); 
			 infowindow.open(map,marker);
		   });
		   bounds.extend(marker.position);
		}
        
        <?php 
            $listeUser = User::ListerPourCarte();
            foreach($listeUser as $key=>$listeLatLng)
            {
                if($key != "")
                {
                    $logins = "";
                    $compte = 0;
                    foreach($listeLatLng as $user)
                    {
                        if($compte > 0)
                        {
                            $logins .= ", ";
                        }
                        $logins .= $user->login;
                        $compte++;
                    }
                    
                    $localisation = $listeLatLng[0]->localisation;
                    
                    $temp = explode(",",$listeLatLng[0]->latlng);
                    $lat = $temp[0];
                    $lng = $temp[1];
                
                    echo "\tcreateMarker(\"".str_replace('"','\"',"<b>".$logins."</b><br />".$localisation)."\",".$lat.",".$lng.",\"".str_replace('"','\"',$localisation)."\");\n";
                }
            }
        ?>
		
    </script>
</div>