<div class='principal_sans_pub'>
	<h1 style='text-align:center;'>Gestion des Protections</h1>
    <div style='text-align:center;'><a href='index.php?ctrl=adminProtections&action=ajouter'><input class="bouton" type='button' value='Ajouter une protection'/></a></div>
    <br>
    <div style='text-align:center;' >Liste des protections de type : <select id='type_protection' onchange='maj_type_protection();'> 
        <option value='toutes'>Toutes</option>
    <?php 
        foreach($types_protection as $type_protection)
        {
            echo "<option value='".$type_protection."' >".$type_protection."</option>";
        }                    
    ?>
    </select> <input type='checkbox' onchange='debase()' id='debase' />Protections de base uniquement &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='checkbox' onchange='officiel()' id='officiel' />Protections officielles uniquement</div><br>
    <div id='nbre_protection' style='text-align:center;'></div>
	 <?php 
        $user;
        if(!isset($user))
        {
            $user = unserialize($_SESSION["birdibeuk_user"]);
        }
        
        $protectionsUser = $user->ListerProtections();
    ?>
    <table class='default'>
        <tr>
            <th>Nom</th><th>Prix</th><th>PR</th>
            <th>Rupture</th><th>Modifications</th><th>Nbr possesseur</th><th>De base</th><th>Officiel</th><th>Actions</th>
        </tr>
        <?php 
            $max_id = 0;
            foreach($protections as $protection)
            {
                if($protection->ID > $max_id)
                {
                    $max_id = $protection->ID;
                }
                
                echo "<tr id='TR_PROTECTION_".$protection->ID."'>";
                    echo "<td>".$protection->NOM."</td>";
                    echo "<td>".$protection->PRIX."</td>";
                    echo "<td>".$protection->PR."</td>";                    
                    echo "<td>".$protection->RUP."</td>";
                    echo "<td>".$protection->modif()."</td>";
                    echo "<td>".$protection->compte."</td><!--".$protection->TYPE."-->";
                    echo "<td>".$protection->debase."</td>";
                    echo "<td>".$protection->officiel."</td>";
                    if($protection->debase)
                    {
                        echo "<!--debase-->";
                    }
                    if($protection->officiel)
                    {
                        echo "<!--officiel-->";
                    }
                    echo "<td>";
					echo "<a href='index.php?ctrl=adminProtections&action=modifier&id=".$protection->ID."'><img style='width:20px;height:20px;margin-bottom:5px;' src='image/pencil.png'/></a>";
					if($user->superadmin || in_array($protection,$protectionsUser))
                    {
						echo "<a href='index.php?ctrl=adminProtections&action=supprimer&id=".$protection->ID."'><img style='width:30px;height:30px;' src='image/delete.png'/></a>";
					}
					echo "</td>";
                echo "<tr>";
            }        
        ?>
    </table>	
</div>
<script>
    var max_protection = <?php echo $max_id;?>;
    
    function maj_type_protection()
    {
        nbre_protection=0;
        for(a=0;a<= max_protection;a++)
        {
            if($('#TR_PROTECTION_'+a).length)
            {                
                if($("#type_protection").val() == "toutes")
                {
                    //raf du type
                    if($('#debase').is(':checked'))
                    {
                        if(-1 != $("#TR_PROTECTION_"+a).html().indexOf("debase") )
                        {
                            if($('#officiel').is(':checked'))
                            {
                                if(-1 != $("#TR_PROTECTION_"+a).html().indexOf("<!--officiel") )
                                {
                                    $("#TR_PROTECTION_"+a).show();
                                    nbre_protection++;
                                }
                                else
                                {
                                    $("#TR_PROTECTION_"+a).hide();
                                }
                            }
                            else
                            {
                                $("#TR_PROTECTION_"+a).show();
                                nbre_protection++;
                            }
                        }
                        else
                        {
                            $("#TR_PROTECTION_"+a).hide();
                        }
                    }
                    else
                    {
                        if($('#officiel').is(':checked'))
                        {
                            if(-1 != $("#TR_PROTECTION_"+a).html().indexOf("<!--officiel") )
                            {
                                $("#TR_PROTECTION_"+a).show();
                                nbre_protection++;
                            }
                            else
                            {
                                $("#TR_PROTECTION_"+a).hide();
                            }
                        }
                        else
                        {
                            $("#TR_PROTECTION_"+a).show();
                            nbre_protection++;
                        }
                    }
                }
                else
                {
                    if(-1 != $("#TR_PROTECTION_"+a).html().indexOf("<!--"+$("#type_protection").val()) )
                    {
                        if($('#debase').is(':checked'))
                        {
                            if(-1 != $("#TR_PROTECTION_"+a).html().indexOf("debase") )
                            {
                                if($('#officiel').is(':checked'))
                                {
                                    if(-1 != $("#TR_PROTECTION_"+a).html().indexOf("<!--officiel") )
                                    {
                                        $("#TR_PROTECTION_"+a).show();
                                        nbre_protection++;
                                    }
                                    else
                                    {
                                        $("#TR_PROTECTION_"+a).hide();
                                    }
                                }
                                else
                                {
                                    $("#TR_PROTECTION_"+a).show();
                                    nbre_protection++;
                                }
                            }
                            else
                            {
                                $("#TR_PROTECTION_"+a).hide();
                            }
                        }
                        else
                        {
                            if($('#officiel').is(':checked'))
                            {
                                if(-1 != $("#TR_PROTECTION_"+a).html().indexOf("<!--officiel") )
                                {
                                    $("#TR_PROTECTION_"+a).show();
                                    nbre_protection++;
                                }
                                else
                                {
                                    $("#TR_PROTECTION_"+a).hide();
                                }
                            }
                            else
                            {
                                $("#TR_PROTECTION_"+a).show();
                                nbre_protection++;
                            }
                        }
                    }
                    else
                    {
                        $("#TR_PROTECTION_"+a).hide();
                    }
                }  
            }            
        }
        if(parseInt(nbre_protection) > 1)
        {
            $("#nbre_protection").html((nbre_protection)+" protections");
        }
        else
        {
            $("#nbre_protection").html((nbre_protection)+" protection");
        }        
    }
    maj_type_protection()
    
    function debase()
    {
        maj_type_protection();
    }
    
    function officiel()
    {
        maj_type_protection();
    }
</script>