<div class='principal_avec_pub'>
	<h1 style='text-align:center'>
        <u><?php echo $compagnie->nom ;?></u>
    </h1>
    <table style='margin:auto;background-image:url("image/bg3.png");border:1px #990000 solid;padding:5px;'>
            <tr>
                <th colspan='5'><h2>Aventuriers</h2></th>
            </tr>
            <tr>
                <th style='width:200px;text-align:left;'><u>Nom</u></th>
                <th style='width:100px;text-align:left;'><u>Origine</u></th>
                <th style='width:100px;text-align:left;'><u>Métier</u></th>
                <th style='width:50px;text-align:left;'><u>Niveau</u></th>
                <th style='width:50px;text-align:center;'><u>Fiches</u></th>
            </tr>
            <?php
                foreach($compagnie->membres as $aventurier)
                {
                    echo "<tr>";
                    echo "  <td style='border-bottom:1px #900000 solid;'>".$aventurier->NOM."</td>
                            <td style='border-bottom:1px #900000 solid;'>".$aventurier->ORIGINE."</td>
                            <td style='border-bottom:1px #900000 solid;' >".$aventurier->METIER."</td>
                            <td style='text-align:center;border-bottom:1px #900000 solid;' >".$aventurier->NIVEAU."</td>  ";
                            ?>
                            <td style='text-align:center;border-bottom:1px #900000 solid;'>
                                    <a href='index.php?ctrl=voirFicheAventurier&aventurier=<?php echo $aventurier->ID; ?>' ><img src='image/icon/scroll.png' style='width:25px;height:25px;' title='fiche officielle' /></a>
                                    <a href='index.php?ctrl=voirFicheAventurier2&aventurier=<?php echo $aventurier->ID; ?>' ><img src='image/icon/scroll2.png' style='width:25px;height:25px;' title='fiche "donjon facile"' /></a>
                                <?php
                                if($aventurier->METIER->NOM != "Mage" && $aventurier->METIER->NOM != "Pretre")
                                {
                                    if($aventurier->possedeCompetence(new Competence(47)) || $aventurier->METIER->NOM == "Ranger" || $aventurier->METIER->NOM == "Voleur" || $aventurier->METIER->NOM == "Assassin")
                                    {
                                        ?>
                                            <a href='index.php?ctrl=voirFicheAventurier3&aventurier=<?php echo $aventurier->ID; ?>' ><img src='image/icon/scroll3.png' title='fiche sur deux pages' style='width:25px;height:25px;' /></a>
                                        <?php
                                    }
                                    else
                                    {
                                        ?>
                                            <a href='index.php?ctrl=voirFicheAventurier5&aventurier=<?php echo $aventurier->ID; ?>' ><img src='image/icon/scroll3.png' title='fiche sur deux pages' style='width:25px;height:25px;' /></a>
                                        <?php
                                    }
                                }
                                else
                                {
                                    ?>
                                        <a href='index.php?ctrl=voirFicheAventurier4&aventurier=<?php echo $aventurier->ID; ?>' ><img src='image/icon/scroll3.png' title='fiche sur deux pages' style='width:25px;height:25px;' /></a>
                                    <?php
                                }
                                ?>
                                <?php
                                    if(isset($_SESSION["birdibeuk_user"]))
                                    {
                                        if(!isset($user))
                                        {
                                            $user = unserialize($_SESSION["birdibeuk_user"]);
                                        }

                                        if($user->id == $aventurier->idjoueur || $user->superadmin)
                                        {
                                            echo "&nbsp;&nbsp;<a href='index.php?ctrl=ficheRapide&action=modification&id_aventurier=".$aventurier->ID."&codeacces=".$aventurier->codeacces."' >Modifier</a>";
                                            echo "&nbsp;&nbsp;<a href='index.php?ctrl=ficheRapide&action=supprimer&id_aventurier=".$aventurier->ID."&codeacces=".$aventurier->codeacces."' >Supprimer</a>";
                                        }
                                    }
                                ?>
                                </td>
<?php
                    echo "</tr>";
                }
            ?>
        </table>
        <?php

        if(userEstConnecte())
        {
            if(!isset($user))
            {
                $user = unserialize($_SESSION["birdibeuk_user"]);
            }

            if($user->AAccesACompagnie($compagnie))
            {
                ?>
                    <form action='index.php?ctrl=outilsMJ&action=fiche_MJ_aventuriers_creation' method='post'>
                        <?php
                            $maxID = -1;
                            foreach($compagnie->membres as $aventurier)
                            {
                                echo "<input type='hidden' name='aventurier".$aventurier->ID."' value='1' />";
                                if($aventurier->ID > $maxID)
                                {
                                    $maxID = $aventurier->ID;
                                }
                            }
                        ?>
                        <input type='hidden' value='<?php echo $maxID; ?>' name='maxID' />
                        <div style='text-align:center' ><input type='submit' value='Fiche MJ de cette compagnie'></div>
                    </form>
                <?php
            }
        }
        ?>
</div>