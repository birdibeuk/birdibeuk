<div class='principal_sans_pub'>
	<h1 style='text-align:center;'>Gestion des Mobs</h1>
    <div style='text-align:center;'><a href='index.php?ctrl=adminMobs&action=ajouter'><input class="bouton" type='button' value='Ajouter un ennemi'/></a></div>
    <br>
    <div style='text-align:center;' >Liste des mobs de type : <select id='type_mob' onchange='maj_type_mob();'> 
        <option value='tous'>Tous</option>
    <?php 
        foreach($types_mob as $type_mob)
        {
            echo "<option value='".$type_mob."' >".$type_mob."</option>";
        }                    
    ?>
    </select> 
    </div>
    <div id='nbre_mob' style='text-align:center;'></div>
    <table class='default'>
        <tr>
            <th>Nom</th><th>Type</th><th>AT</th><th>PRD</th><th>EV</th><th>PR</th><th>PI</th>
            <th>Type d'arme</th><th>COU</th><th>Résist. Mag.</th><th>Classe XP</th><th>Note</th>
            <th>Niveau min</th><th>Niveau max</th><th>PO</th><th>Humanoide</th><th>Action</th>
        </tr>
        <?php
            $max_id = 0;
            foreach($mobs as $mob)
            {
                if($mob->id_mob > $max_id)
                {
                    $max_id = $mob->id_mob;
                }                
                echo "<tr id='TR_MOB_".$mob->id_mob."'>";
                $temp = explode('/',$mob->type_mob);
                foreach($temp as $type)
                {
                    echo "<!--".$type."-->";
                }
                    echo "<td>".$mob->nom."</td>";
                    echo "<td>".$mob->type_mob."</td>";
                    echo "<td>".$mob->at."</td>";
                    echo "<td>".$mob->prd."</td>";
                    echo "<td>".$mob->ev."</td>";
                    echo "<td>".$mob->pr."</td>";
                    echo "<td>".$mob->pi."</td>";
                    echo "<td>".$mob->type_arme."</td>";
                    echo "<td>".$mob->cou."</td>";
                    echo "<td>".$mob->rm."</td>";
                    echo "<td>".$mob->xp."</td>";
                    echo "<td>".$mob->note."</td>";
                    echo "<td>".$mob->niv_min."</td>";
                    echo "<td>".$mob->niv_max."</td>";
                    echo "<td>";
                    if($mob->po == 0){echo "Non";}else{echo $mob->po;}
                    echo "</td>";
                    echo "<td>";
                    if($mob->humanoide == 1){echo "Oui";}else{echo "Non";}
                    echo "</td>";
                    
                    echo "<td> ";
                        echo "<a href='index.php?ctrl=adminMobs&action=modifier&id=".$mob->id_mob."'><img style='width:20px;height:20px;margin-bottom:5px;' src='image/pencil.png'/></a> ";
                        
                        if($user->superadmin)
                        {
                            echo "<a href='index.php?ctrl=adminMobs&action=supprimer&id=".$mob->id_mob."'><img style='width:30px;height:30px;' src='image/delete.png'/></a>";
                        }                        
                    echo "</td>";
                echo "</tr>";
            }
        ?>
    </table>
</div>
<script>
    var max_mob = <?php echo $max_id;?>;
    
    function maj_type_mob()
    {        
        nbre_mob=0;
        for(a=0;a<= max_mob;a++)
        {
            if($('#TR_MOB_'+a).length)
            {
                if(-1 != $('#TR_MOB_'+a).html().indexOf("<!--"+$("#type_mob").val()) || $("#type_mob").val() == "tous")
                {
                    $("#TR_MOB_"+a).show(); 
                    nbre_mob++;       
                }
                else
                {
                    $("#TR_MOB_"+a).hide();
                }
            }            
        }
        
        if(parseInt(nbre_mob) > 1)
        {
            $("#nbre_mob").html((nbre_mob)+" ennemis");
        }
        else
        {
            $("#nbre_mob").html((nbre_mob)+" ennemi");
        }        
    }
    maj_type_mob();
</script>