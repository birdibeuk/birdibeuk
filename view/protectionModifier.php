<div class='principal_sans_pub'>
	<h1 style='text-align:center;'>Modifier une protection</h1>
    <form method='post' action='index.php?ctrl=adminProtections&action=modifier_action'>
        <table class='formulaire' style='width:600px;text-align:left;'>
            <input type='hidden' id='ID' value='<?php echo $protection->ID; ?>' name='ID' />
            <tr><td style='width:120px;'><label for='NOM'>NOM :</label></td><td style='width:520px;'><input style='width:420px;' type='text' id='NOM' value='<?php echo str_replace("'","&apos;",$protection->NOM); ?>' name='NOM' /></td></tr>
            <tr><td><label for='NOM_COURT'>NOM_COURT :</label></td><td><input type='text' id='NOM_COURT' value='<?php echo $protection->NOM_COURT; ?>' name='NOM_COURT' /></td></tr>
            <tr><td><label for='PRIX'>PRIX :</label></td><td><input type='text' id='PRIX' value='<?php echo $protection->PRIX; ?>' name='PRIX' /></td></tr>
            <tr><td><label for='PR'>PR :</label></td><td><input type='text' id='PR' value='<?php echo $protection->PR; ?>' name='PR' /></td></tr>
            <tr><td><label for='RUP'>RUP :</label></td><td><input type='text' id='RUP' value='<?php echo $protection->RUP; ?>' name='RUP' /></td></tr>
            <tr><td><label for='AT'>AT :</label></td><td><input type='text' id='AT' value='<?php echo $protection->AT; ?>' name='AT' /></td></tr>
            <tr><td><label for='PRD'>PRD :</label></td><td><input type='text' id='PRD' value='<?php echo $protection->PRD; ?>' name='PRD' /></td></tr>
            <tr><td><label for='COU'>COU :</label></td><td><input type='text' id='COU' value='<?php echo $protection->COU; ?>' name='COU' /></td></tr>
            <tr><td><label for='INT'>INT :</label></td><td><input type='text' id='INT' value='<?php echo $protection->INT; ?>' name='INT' /></td></tr>
            <tr><td><label for='CHA'>CHA :</label></td><td><input type='text' id='CHA' value='<?php echo $protection->CHA; ?>' name='CHA' /></td></tr>
            <tr><td><label for='AD'>AD :</label></td><td><input type='text' id='AD' value='<?php echo $protection->AD; ?>' name='AD' /></td></tr>
            <tr><td><label for='FOR'>FOR :</label></td><td><input type='text' id='FOR' value='<?php echo $protection->FOR; ?>' name='FOR' /></td></tr>
            <tr><td><label for='SPECIAL'>SPECIAL :</label></td><td><input type='text' id='SPECIAL' value='<?php echo $protection->SPECIAL; ?>' name='SPECIAL' /></td></tr>
            <tr><td><label for='qualite'>qualite :</label></td><td><input type='text' id='qualite' value='<?php echo $protection->qualite; ?>' name='qualite' /></td></tr>
            <tr>
                <td><label for='TYPE'>TYPE :</label></td>
                <td>
                    <select name='TYPE'>
                        <?php 
                            foreach($types_protection as $type_protection)
                            {
                                echo "<option value='".$type_protection."' "; 
                                if($protection->TYPE == $type_protection){echo " selected='selected' ";}
                                echo " >".$type_protection."</option>";
                            }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td><label for='officiel'>officiel :</label></td>
                <td><input type='checkbox' id='officiel' <?php if($protection->officiel){echo "checked"; } ?> name='officiel' /></td>
            </tr>
             <tr>
                <td><label for='debase'>debase :</label></td>
                <td><input type='checkbox' id='debase' <?php if($protection->debase){echo "checked"; } ?> name='debase' /></td>
            </tr>
            <tr>
                <td colspan='2' style='text-align:center' ><input type='submit' value='Modifier' /></td>
            </tr>
        </table>
    </form>
</div>