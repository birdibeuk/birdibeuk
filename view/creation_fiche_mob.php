<div class='principal_avec_pub'>
    <style>
        .tableMob
        {
            border-collapse:collapse;
            background-image:url('image/bg4.png');
            margin:10px;
            width:300px;
            border: 3px #900000 solid;
        }
        
        .tableMob td
        {
            border: 1px #900000 solid;
            padding:5px;
            text-align:center;
        }
    </style>
    <div style='text-align:center;'>
        <a target='blank_' href='view/creation_fiche_mob_imprimable.php'>
            <input type='button' value='version imprimable des tableaux' style='width:250px;'/>
        </a>
        <a target='blank_' href='view/creation_fiche_mob_imprimable_image.php'>
            <input type='button' value='version imprimable des images' style='width:250px;'/>
        </a>
    </div>
    <table>
        <tr>
        <?php
        $noms = array();
        foreach($mobs as $mob)
        {
            if(array_key_exists($mob->nom,$noms))
            {
                $noms[$mob->nom]++;
                $mob->nom = $mob->nom." ".$noms[$mob->nom];
            }
            else
            {
                $noms[$mob->nom] = 1;
            }
            echo "<td>";
            
            if(isset($_POST["format"]))
            {
                if($_POST["format"] == "image")
                {
                    $mob->image();
                }
                else
                {
                    $mob->printInfo();
                }
            }
            else
            {
                $mob->printInfo();
                echo "<br>";
                $mob->image();
            }
            
            
            
            echo "</td>";
            $compte++;
            if($compte == 3)
            {
                echo "</tr><tr>";
                $compte = 0;             
            }
        }
    ?>
        </tr>
    </table>
</div>
