<?php 
    session_start();
?>
<!doctype html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title></title>
		<meta name="description" content="">
		<meta name="author" content="Favay Thomas">
	</head>
	<body>
    <?php 
        include(__DIR__."/../config/param.php");
        include(__DIR__."/../model/autoloader.php");
        include(__DIR__."/../model/GeneralFunctions.php");
        $mobs = unserialize($_SESSION["mobs"]);
        ?>
        <style>
            img
            {
                width:250px;
                height:370px;
            }
        </style>
        <table>
            <tr>
            <?php       
            $noms = array();
            foreach($mobs as $mob)
            {
                if(array_key_exists($mob->nom,$noms))
                {
                    $noms[$mob->nom]++;
                    $mob->nom = $mob->nom." ".$noms[$mob->nom];
                }
                else
                {
                    $noms[$mob->nom] = 1;
                }
                echo "<td>";
                $mob->image();
                echo "</td>";
                $compte++;
                if($compte == 3)
                {
                    echo "</tr><tr>";
                    $compte = 0;             
                }
            }
        ?>
            </tr>
        </table>
	</body>
</html>
