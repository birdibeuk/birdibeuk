<div class='principal_avec_pub'>
    <h1 style='text-align:center;'>Choisir une origine et un métier</h1>
    <div>
    <u>Voici vos résultats : </u><br><br>
    <?php des($aventurier->COU - 7); ?>
    <?php des($aventurier->INT - 7); ?>
    <?php des($aventurier->CHA - 7); ?>
    <?php des($aventurier->AD - 7); ?>
    <?php des($aventurier->FO - 7); ?>
    </div>
    
    <div style='margin-top:70px;'>
    <u>Ce qui vous donne comme caractéristiques :</u><br />
    
    <table>
    <tr><td>Courage <?php getValeurCaracM($aventurier->COU); ?> : </td><td> <?php echo $aventurier->COU; ?> </td></tr>
    <tr><td>Intelligence <?php getValeurCaracF($aventurier->INT); ?> : </td><td> <?php echo $aventurier->INT; ?> </td></tr>
    <tr><td>Charisme <?php getValeurCaracM($aventurier->CHA); ?> : </td><td> <?php echo $aventurier->CHA; ?> </td></tr>
    <tr><td>Adresse <?php getValeurCaracF($aventurier->AD); ?> : </td><td> <?php echo $aventurier->AD; ?>	</td></tr>
    <tr><td>Force <?php getValeurCaracF($aventurier->FO); ?> : </td><td> <?php echo $aventurier->FO; ?> </td></tr>
    </table>
    </div>
    <br>
    <div style='text-align:center;'>
        Merci de remplir la première page du formulaire en vous choisissant un nom et un sexe <br/>
        <form action='index.php' method='get'>
            <input type='hidden' name='etape' value='3' />
            <input type='hidden' name='ctrl' value='nouvelAventurier' />
            <input type='hidden' name='origine' value='3' />
            <input type='hidden' name='metier' value='99' />
            <div class='formulaireParchemin'>
                <br>
                Je me nomme <input class='bouton' type='text' name='nom' id='nom' required /><br><br> Je suis un(e) nain(e) mafieux(se) de sexe 
                <select name='sexe'>
                    <option value='Masculin' >Masculin</option>
                    <option value='Féminin' >Féminin</option>
                </select><br><br>
                <input class='bouton' type='submit' value='sélectionner cette combinaison' />
            </div>
        </form>
    </div>
    <br><br>
    <div>
        Si rien de tout cela ne vous convient et que vous voulez carrément refaire un essai, cliquez ici (même si c'est mal) :<br><br>
        <form method='get' action='index.php'>
            <input type='hidden' value='99' name='metier'>
            <input type='hidden' value='2' name='etape'>
            <input type='hidden' value='nouvelAventurier' name='ctrl'>
            <input class='bouton' type='submit' value='Relancer les dés discrètement telle une fouine furtive du chaos' />
        </form>
    </div>
</div>