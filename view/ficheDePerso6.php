<?php 
session_start();
header ("Content-type: image/png");
$image = imagecreatefromjpeg(__DIR__."/../image/equipement-guerrier.jpg");

/*
$avatar_x_size = getimagesize($avatar )[0];
$avatar_y_size = getimagesize($avatar )[1];

$to_crop_array = array('x' =>0 , 'y' => 0, 'width' => $avatar_x_size, 'height'=> $avatar_y_size);

$avatar = imagecrop($avatar);*/

include(__DIR__."/../config/param.php");
include(__DIR__."/../model/GeneralFunctions.php");
include(__DIR__."/../model/autoloader.php");

$aventurier;
if(isset($_GET["id"]))
{
    $aventurier = new Aventurier($_GET["id"]);
}
else if(isset($_SESSION["birdibeuk_aventurier"]))
{
    $aventurier = unserialize($_SESSION["birdibeuk_aventurier"]);
}
else 
{
    $aventurier = new Aventurier();
}

function urlExist($url) {
    $file_headers = @get_headers($url);
    if($file_headers[0] == 'HTTP/1.1 404 Not Found')
    {return false;}
 
    return true;
}

$noir = imagecolorallocate($image, 0, 0, 0);
$vert = imagecolorallocate($image, 0, 144, 0);
$rouge = imagecolorallocate($image, 144, 0, 0);

$font = __DIR__."/../fonts/VINERITC.TTF"; 
if(isset($_GET["font"]))
{
	$font = __DIR__."/../fonts/".$_GET["font"].".TTF"; 
}

$fontSize = 40;
$fontSizeEquipement = 40;

imagettftext($image, $fontSize, 0, 170, 318, $noir, $font, $aventurier->NOM);

$string = "";
$string_temp = "";
$compte = 0;
$numeroLigne = 0;
$deja_ecrit = array();
$limite_largeur_cadre = 535;

$nbr_munition=0;
$nbr_nourriture=0;
$nbr_grimoire=0;
$nbr_potion=0;
$nbr_poison=0;
$nbrSpecial=0;
$nbr_ingredient=0;
$nbr_bague=0;

foreach($aventurier->equipements as $key=>$equipement)
{
    if($equipement->type == "nourriture")
    {
        $nom = "";
        if($equipement->nombre == 1)
        {
            $nom = $equipement->NOM; 
        }
        
        else
        {
            $nom = "(".$equipement->nombre.")".$equipement->NOM; 
        }
        
        $font_size_temp = $fontSize;
        $dimensions = imagettfbbox($font_size_temp, 0, $font, $nom);
        $lineWidth = $dimensions[2] - $dimensions[0];
        while(($lineWidth = ($dimensions[2] - $dimensions[0])) > 600)
        {
            $font_size_temp = $font_size_temp - 1;
            $dimensions = imagettfbbox($font_size_temp, 0, $font, $nom);
            $lineWidth = $dimensions[2] - $dimensions[0];
        }
        
        imagettftext($image, $font_size_temp, 0, 45, intval(810 + $nbr_nourriture*67), $noir, $font, $nom);            
        $nbr_nourriture++;
    }        
    else if($equipement->type == "potion")
    {
        $amelioration = "";
        $nom = "";
        $index_parenthese = strpos($equipement->NOM,"(");
        if($index_parenthese !== false)
        {
            $amelioration = substr($equipement->NOM,$index_parenthese);
            $nom = substr($equipement->NOM,0,$index_parenthese);
        }
        else
        {
            $amelioration = "";
            $nom = $equipement->NOM;
        }
        
        $font_size_temp = $fontSize;
        imagettftext($image, $font_size_temp, 0, 820, intval(556 + $nbr_potion*60), $noir, $font, $equipement->nombre);
        
        $font_size_temp = $fontSize;
        $dimensions = imagettfbbox($font_size_temp, 0, $font, $amelioration);
        $lineWidth = $dimensions[2] - $dimensions[0];
        while(($lineWidth = ($dimensions[2] - $dimensions[0])) > 500)
        {
            $font_size_temp = $font_size_temp - 1;
            $dimensions = imagettfbbox($font_size_temp, 0, $font, $amelioration);
            $lineWidth = $dimensions[2] - $dimensions[0];
        }
        imagettftext($image, $font_size_temp, 0, 1830, intval(556 + $nbr_potion*60), $noir, $font, $amelioration);
        
        $font_size_temp = $fontSize;
        $dimensions = imagettfbbox($font_size_temp, 0, $font, $nom);
        $lineWidth = $dimensions[2] - $dimensions[0];
        while(($lineWidth = ($dimensions[2] - $dimensions[0])) > 600)
        {
            $font_size_temp = $font_size_temp - 1;
            $dimensions = imagettfbbox($font_size_temp, 0, $font, $nom);
            $lineWidth = $dimensions[2] - $dimensions[0];
        }
        
        imagettftext($image, $font_size_temp, 0, 1100, intval(556 + $nbr_potion*60), $noir, $font, $nom);
        
        $nbr_potion++;
    }
    else if($equipement->type == "poison")
    {
        $amelioration = "";
        $nom = "";
        $index_parenthese = strpos($equipement->NOM,"(");
        if($index_parenthese !== false)
        {
            $amelioration = substr($equipement->NOM,$index_parenthese);
            $nom = substr($equipement->NOM,0,$index_parenthese);
        }
        else
        {
            $amelioration = "";
            $nom = $equipement->NOM;
        }
        
        $font_size_temp = $fontSize;
        imagettftext($image, $font_size_temp, 0, 820, intval(1080 + $nbr_potion*60), $noir, $font, $equipement->nombre);
        
        $font_size_temp = $fontSize;
        $dimensions = imagettfbbox($font_size_temp, 0, $font, $amelioration);
        $lineWidth = $dimensions[2] - $dimensions[0];
        while(($lineWidth = ($dimensions[2] - $dimensions[0])) > 500)
        {
            $font_size_temp = $font_size_temp - 1;
            $dimensions = imagettfbbox($font_size_temp, 0, $font, $amelioration);
            $lineWidth = $dimensions[2] - $dimensions[0];
        }
        imagettftext($image, $font_size_temp, 0, 1830, intval(1080 + $nbr_potion*60), $noir, $font, $amelioration);
        
        $font_size_temp = $fontSize;
        $dimensions = imagettfbbox($font_size_temp, 0, $font, $nom);
        $lineWidth = $dimensions[2] - $dimensions[0];
        while(($lineWidth = ($dimensions[2] - $dimensions[0])) > 600)
        {
            $font_size_temp = $font_size_temp - 1;
            $dimensions = imagettfbbox($font_size_temp, 0, $font, $nom);
            $lineWidth = $dimensions[2] - $dimensions[0];
        }
        
        imagettftext($image, $font_size_temp, 0, 1100, intval(1080 + $nbr_potion*60), $noir, $font, $nom);
        
        $nbr_potion++;
    }
    else if($equipement->COU != 0 || $equipement->INT != 0 || $equipement->CHA != 0|| $equipement->AD != 0 || $equipement->FO != 0)
    {
        $nom = "";
        if($equipement->nombre == 1)
        {
            $nom = $equipement->NOM; 
        }
        else
        {
            $nom = "(".$equipement->nombre.")".$equipement->NOM; 
        }
        $font_size_temp = $fontSizeEquipement;
        $dimensions = imagettfbbox($font_size_temp, 0, $font, $nom);
        $lineWidth = $dimensions[2] - $dimensions[0];
        while(($lineWidth = ($dimensions[2] - $dimensions[0])) > 600)
        {
            $font_size_temp = $font_size_temp - 1;
            $dimensions = imagettfbbox($font_size_temp, 0, $font, $nom);
            $lineWidth = $dimensions[2] - $dimensions[0];
        }
        
        imagettftext($image, $font_size_temp, 0, 190, intval(1732 + $nbrSpecial*63), $noir, $font, $nom);
        imagettftext($image, $font_size_temp, 0, 1490, intval(1732 + $nbrSpecial*63), $noir, $font, $equipement->modifCharac());
        $nbrSpecial++;
    }
}

imagepng($image); 
?>