<div class='principal_sans_pub'>
	<h1 style='text-align:center;'>Ajouter un ennemi</h1>
    <form method='post' action='index.php?ctrl=adminMobs&action=ajouter_action'>
        <table class='formulaire' style='width:600px;text-align:left;'>
            <tr><td style='width:120px;'><label for='NOM'>NOM :</label></td><td style='width:520px;'><input style='width:420px;' type='text' id='nom' value="" name='nom' /></td></tr>
            <tr><td><label for='type_mob'>type de mob :</label></td><td><input type='text' id='type_mob' value='' name='type_mob' /></td></tr>
            <tr><td><label for='at'>AT :</label></td><td><input type='text' id='at' value='8' name='at' /></td></tr>
            <tr><td><label for='prd'>PRD :</label></td><td><input type='text' id='prd' value='8' name='prd' /></td></tr>
            <tr><td><label for='ev'>EV :</label></td><td><input type='text' id='ev' value='10' name='ev' /></td></tr>
            <tr><td><label for='pr'>PR :</label></td><td><input type='text' id='pr' value='1' name='pr' /></td></tr>
            <tr><td><label for='PI'>PI :</label></td><td><input type='text' id='PI' value='1D+2' name='pi' /></td></tr>
            <tr><td><label for='type_arme'>types d'arme pouvant être utilisée :</label></td>
                <td>
                    <?php 
                        $armesMob = explode('/',$mob->type_arme);
                        foreach($types_arme as $type_arme)
                        {
                            echo "<input type='checkbox' name=\"".$type_arme."\">".$type_arme."<br/>";
                        }   
                    ?>
                </td>
            </tr>
            <tr><td><label for='cou'>COU :</label></td><td><input type='text' id='cou' value='8' name='cou' /></td></tr>
            <tr><td><label for='rm'>Resistance magique :</label></td><td><input type='text' id='rm' value='8' name='rm' /></td></tr>
            <tr><td><label for='xp'>Classe XP :</label></td><td><input type='text' id='xp' value='5' name='xp' /></td></tr>
            <tr><td><label for='note'>Note :</label></td><td><textarea id='note' name='note'></textarea></td></tr>
            <tr><td><label for='niv_min'>Niveau min :</label></td><td><input type='text' id='niv_min' value='1' name='niv_min' /></td></tr>
            <tr><td><label for='niv_max'>Niveau max :</label></td><td><input type='text' id='niv_max' value='3' name='niv_max' /></td></tr>
            <tr><td><label for='po'>PO :</label></td><td><input type='text' id='po' value='0' name='po' /></td></tr>
            
            <tr><td><label for='humanoide'>Humanoide :</label></td><td><select id='humanoide' name='humanoide'>
                <option value='1' <?php if( $mob->humanoide){echo "selected"; }; ?>>Oui</option>
                <option value='0' <?php if(!$mob->humanoide){echo "selected"; }; ?>>Non</option>
            </select>
            
            <tr>
                <td colspan='2' style='text-align:center' ><input type='submit' value='Ajouter' /></td>
            </tr>
        </table>
    </form>
</div>