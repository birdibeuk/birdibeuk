<div class='principal_avec_pub'>
    <h1 style='text-align:center;'>Spécificité de l'Ogre.</h1>
    <div>
        SUPER-BOURRIN : L'Ogre a le droit de retrancher jusqu'à 3 points à son score de base en ATTAQUE et/ou en PARADE
        pour en faire un bonus de dégâts (au corps à corps, toutes armes confondues). Ainsi il peut avoir +3 en dégâts en plus de ses
        autres bonus de FORCE, mais son côté bourrin le rend maladroit. Le bonus est à choisir en début de carrière et ne pourra
        être modifié par la suite.
    </div><br>
    <style>
        select
        {
            font-size: 20px;
        }
    </style>
    <?php 
        $aventurier->printAsTable();
    ?>
    <form action='index.php' method='get' id='formulaire'>
        <input type='hidden' name='etape' value='5' />
        <input type='hidden' name='ctrl' value='nouvelAventurier' />  
        Convertir <select name='competenceRetireeAT' id='competenceRetireeAT' onChange='majPI()'>
            <option value='0'>0</option>
            <option value='1'>1</option>
            <option value='2'>2</option>
            <option value='3'>3</option>
        </select> point(s) d'attaque et <select name='competenceRetireePRD' id='competenceRetireePRD' onChange='majPI()'>
            <option value='0'>0</option>
            <option value='1'>1</option>
            <option value='2'>2</option>
            <option value='3'>3</option>
        </select> point(s) de parade en <span id='bonus_degat'>0</span> points de dégâts.<br>
        <input class='bouton' type='button' onclick='envoyer()' value='confirmer' style='width:150px;'/>
    </form><br><br>  
</div>
<script>
    function majPI()
    {
        $("#bonus_degat").html( parseInt(parseInt($("#competenceRetireeAT").val()) + parseInt($("#competenceRetireePRD").val())));
    }
    
    function envoyer()
    {
        if( parseInt(parseInt($("#competenceRetireeAT").val()) + parseInt($("#competenceRetireePRD").val())) > 3)
        {
            alert("Vous ne pouvez convertir que trois points au maximum.");
        }
        else
        {
            $("#formulaire").submit();
        }
    }
</script>