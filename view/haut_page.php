<!doctype html>
<html lang="fr">
	<head>
        <link rel="icon" type="image/png" href="favicon.ico" />
        <!--[if IE]><link rel="shortcut icon" type="image/x-icon" href="favicon.ico" /><![endif]-->
		<meta charset="utf-8">
		<title>Birdibeuk - Création de personnage pour le JDR Naheulbeuk !</title>
		<meta name="description" content="">
		<meta name="author" content="Favay Thomas">
        
        <link rel="stylesheet" href="asset/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="style/default.css">
		<link rel="stylesheet" href="style/news.css">
		<link rel="stylesheet" href="style/menu.css">
		<link rel="stylesheet" href="style/form.css">
		<link rel="stylesheet" href="style/pub.css">
		<link rel="stylesheet" href="style/links.css">
		<link rel="stylesheet" href="style/dice.css">
		<link rel="stylesheet" href="style/table.css">
        
        <!--<link rel="stylesheet" href="asset/bootstrap/css/bootstrap-theme.min.css">-->
		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="asset/bootstrap/js/bootstrap.min.js"></script>
        
        <script type="text/javascript" src="js/pdf.js"></script>
        <script type="text/javascript" src="js/general.js"></script>
	</head>
	<body>
		<div class='content'>
			<a href='index.php'>
				<div class='header'>
					<table style='width:100%;'>
                        <tr>
                            <td>
                                <img src='image/icone-tableau.png' style='margin-bottom:-40px;margin-right:50px;'/>
                            </td>   
                            <td>  
                                Bienvenue dans la bibliothèque d'archives de la Caisse Des Donjons de la terre de Fangh
                            </td>   
                            <td> 
                                <img src='image/icone-tableau2.png' style='margin-bottom:-40px;margin-left:50px;'/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan='3'>
                                <span class='petit'>(depuis l'an 425 du premier âge)</span>
                            </td>
                        </tr>
                    </table>
				</div>
			</a>