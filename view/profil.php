<div class='principal_avec_pub'>
    <h3>Fiche d'archive officielle de la CDD sur le joueur <?php 
    
    echo $user_proprietaire->login; 
    if($modificationAutorisee)
    {
        ?>
        <a href='index.php?ctrl=editProfil'>&nbsp;&nbsp;&nbsp;&nbsp;<img src='image/modify.png' style='width:20px;height:20px;' /></a>
        <?php
    }
    ?>
</h3>
<p>Date d'inscription : <?php echo $user_proprietaire->date_inscription; ?></p>
<p>Dernière visite : <?php echo $user_proprietaire->date_last_visit; ?></p>
<?php
    if($user_proprietaire->mail != "")
    {
        echo "<p>Adresse email : ".str_replace("@","(at)",$user_proprietaire->mail)."</p>";
    }
    
    echo "<p>Est un MJ : ";
    if($user_proprietaire->mj)
    {
        echo "Oui";
    }
    else
    {
        echo "Non";
    }
    echo "</p>";
    
    if(!empty($user_proprietaire->description))
    {
        echo "<p>Description : <br>".$user_proprietaire->description."</p>";
     }
    
    $compte = 0;
    foreach($user_proprietaire->aventuriers as $aventurier)
    {
        if($compte==0)
        {
            echo "<p style='text-align:center;' >Les aventuriers de ce joueur</p>";
            ?>
            <style>
                td
                {
                    text-align:center;
                }
                
            </style>
            <table style='margin:auto;background-image:url("image/bg3.png");border: 3px #900000 solid;-webkit-border-radius: 20px;
        -moz-border-radius: 20px;
        border-radius: 20px;'>
            <tr>
                <th style='width:200px'><u><a href='index.php?ctrl=archiveAventurier&ordre=<?php 
                    trouveBonFiltre("NOM");
                    if(isset($_GET["nom"])){echo "&nom=".$_GET["nom"];}
                ?>' >Nom</a></u></th>
                <th style='width:100px'><u><a href='index.php?ctrl=archiveAventurier&ordre=<?php 
                    trouveBonFiltre("SEXE");
                    if(isset($_GET["nom"])){echo "&nom=".$_GET["nom"];}
                ?>'>Sexe</a></u></th>
                <th style='width:100px'><u><a href='index.php?ctrl=archiveAventurier&ordre=<?php 
                    trouveBonFiltre("ID_METIER");
                    if(isset($_GET["nom"])){echo "&nom=".$_GET["nom"];}
                ?>'>Métier</a></u></th>
                <th style='width:100px'><u><a href='index.php?ctrl=archiveAventurier&ordre=<?php 
                    trouveBonFiltre("ID_ORIGINE");
                    if(isset($_GET["nom"])){echo "&nom=".$_GET["nom"];}
                ?>'>Origine</a></u></th>
                <th style='width:50px'><u><a href='index.php?ctrl=archiveAventurier&ordre=<?php 
                    trouveBonFiltre("NIVEAU");
                    if(isset($_GET["nom"])){echo "&nom=".$_GET["nom"];}
                ?>'>Niveau</a></u></th>
                <th style='width:30px'><u><a href='index.php?ctrl=archiveAventurier&ordre=<?php 
                    trouveBonFiltre("EV");
                    if(isset($_GET["nom"])){echo "&nom=".$_GET["nom"];}
                ?>'>EV</a></u></th>
                <th style='width:30px'><u><a href='index.php?ctrl=archiveAventurier&ordre=<?php 
                    trouveBonFiltre("EA");
                    if(isset($_GET["nom"])){echo "&nom=".$_GET["nom"];}
                ?>'>EA</a></u></th>
                <th style='width:30px'><u><a href='index.php?ctrl=archiveAventurier&ordre=<?php 
                    trouveBonFiltre("COU");
                    if(isset($_GET["nom"])){echo "&nom=".$_GET["nom"];}
                ?>'>COU</a></u></th>
                <th style='width:30px'><u><a href='index.php?ctrl=archiveAventurier&ordre=<?php 
                    trouveBonFiltre("INT");
                    if(isset($_GET["nom"])){echo "&nom=".$_GET["nom"];}
                ?>'>INT</a></u></th>
                <th style='width:30px'><u><a href='index.php?ctrl=archiveAventurier&ordre=<?php 
                    trouveBonFiltre("CHA");
                    if(isset($_GET["nom"])){echo "&nom=".$_GET["nom"];}
                ?>'>CHA</a></u></th>
                <th style='width:30px'><u><a href='index.php?ctrl=archiveAventurier&ordre=<?php 
                    trouveBonFiltre("AD");
                    if(isset($_GET["nom"])){echo "&nom=".$_GET["nom"];}
                ?>'>AD</a></u></th>
            <th style='width:30px'><u><a href='index.php?ctrl=archiveAventurier&ordre=<?php 
                trouveBonFiltre("FO");
                if(isset($_GET["nom"])){echo "&nom=".$_GET["nom"];}
            ?>'>FO</a></u></th>
            <th style='width:30px'><u><a href='index.php?ctrl=archiveAventurier&ordre=<?php 
                trouveBonFiltre("AT");
                if(isset($_GET["nom"])){echo "&nom=".$_GET["nom"];}
            ?>'>AT</a></u></th>
            <th style='width:30px'><u><a href='index.php?ctrl=archiveAventurier&ordre=<?php 
                trouveBonFiltre("PRD");
                if(isset($_GET["nom"])){echo "&nom=".$_GET["nom"];}
            ?>'>PRD</a></u></th>
            <th style='width:180px'></th>
        </tr>
        <?php
        }
        include("view/aventurierLigne.php");
        $compte++;
    }
    
    if($compte > 0)
    {
        echo "</table>";
    }
    
    if($user_proprietaire->localisation != "")
    {
        ?>
        <style>
        #map_canvas 
        {
            height: 700px;
            width: 900px;
            margin:50px auto;
        }
        </style>
        <div id="map_canvas" style='border: 3px #900000 solid;-webkit-border-radius: 20px;
    -moz-border-radius: 20px;
    border-radius: 20px;'></div>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script> 
    <script>               
        var geocoder =  new google.maps.Geocoder();
        
		var infowindow = new google.maps.InfoWindow();
		var latlng = new google.maps.LatLng(46.52359,2.33288);
		var mapOptions = {
                zoom: 6,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			
		var geo = new google.maps.Geocoder(); 
		var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
		var bounds = new google.maps.LatLngBounds();
		
		function createMarker(add,lat,lng,txt) {
		   var contentString = add;
		   var marker = new google.maps.Marker({
			 position: new google.maps.LatLng(lat,lng),
			 map: map,
			 title: txt,
			 zIndex: Math.round(latlng.lat()*-100000)<<5
		   });

		  google.maps.event.addListener(marker, 'click', function() {
			 infowindow.setContent(contentString); 
			 infowindow.open(map,marker);
		   });
		   bounds.extend(marker.position);
		}
        
        <?php 
            if($user_proprietaire->latlng != "")
            {
                $temp = explode(",",$user_proprietaire->latlng);
                $lat = $temp[0];
                $lng = $temp[1];
                echo "\tcreateMarker(\"".str_replace('"','\"',"<b>".$user_proprietaire->login."</b><br />".$user_proprietaire->localisation)."\",".$lat.",".$lng.",\"".str_replace('"','\"',$user_proprietaire->localisation)."\");\n";
            }
        ?>
		
    </script>
    <?php
    }
    
?>
</div>