<div class='principal_avec_pub'>    
	<form action='index.php?ctrl=gestionCompagnies&action=actionAjouter' method='post'>
    <p style='text-align:center'>
        Vous êtes sur le point de créer une nouvelle compagnie !<br> (Je sais, l'émotion de ce moment magique est insoutenable.)<br>
        Choisissez le nom de cette compagnie : <input type='text' name='nom' placeholder='Nom de la compagnie' required /> 
    </p>
    <p style='text-align:center'>Ensuite, sélectionnez les membres :</p>   
    <div style='text-align:center'>filtre : <input type='text' id='filtre' placeholder='filtre...' onkeyup='filtrer();' /></div>    
        
        <?php 
            if(count($mesAventuriers) > 0)
            {
                ?>
                <table style='margin:auto;'>
                    <tr>
                        <th colspan='5'><b><u>Vos aventuriers</u></b><br></th>                
                    </tr>
                    <tr>
                        <th style='width:10px;text-align:left;'></th>
                        <th style='width:200px;text-align:left;'><u>Nom</u></th>           
                        <th style='width:100px;text-align:left;'><u>Origine</u></th>
                        <th style='width:100px;text-align:left;'><u>Métier</u></th>
                        <th style='width:50px;text-align:left;'><u>Niveau</u></th>                
                    </tr>
                    <?php 
                        $maxID = 0;
                        foreach($mesAventuriers as $aventurier)
                        {
                            echo "<tr class='ligneAv'>";
                            echo "  <td><input type='checkbox' name='aventurier_".$aventurier->ID."' value='".$aventurier->ID."' /></td>
                                    <td>".$aventurier->NOM."</td>
                                    <td >&nbsp;".$aventurier->ORIGINE."</td>
                                    <td >&nbsp;".$aventurier->METIER."</td>
                                    <td style='text-align:center;'>&nbsp;".$aventurier->NIVEAU."</td>  ";                          
                            echo "</tr>";
                            if($aventurier->ID > $maxID)
                            {
                                $maxID = $aventurier->ID;
                            }
                        }
                    ?>
                </table>
                <?php
            }
        ?>
        
        <br><br>
        <table style='margin:auto;'>
            <tr>
                <th style='width:10px;text-align:left;'></th>
                <th style='width:200px;text-align:left;'><u>Nom</u></th>           
                <th style='width:100px;text-align:left;'><u>Origine</u></th>
                <th style='width:100px;text-align:left;'><u>Métier</u></th>
                <th style='width:50px;text-align:left;'><u>Niveau</u></th>                
            </tr>
            <div style='text-align:center;'><input type='submit' value='Créer la compagnie' style='width:400px;'/></div>
            <?php 
                $maxID = 0;
                foreach($aventuriers as $aventurier)
                {
                    echo "<tr class='ligneAv'>";
                    echo "  <td><input type='checkbox' name='aventurier_".$aventurier->ID."' value='".$aventurier->ID."' /></td>
                            <td>".$aventurier->NOM."</td>
                            <td >&nbsp;".$aventurier->ORIGINE."</td>
                            <td >&nbsp;".$aventurier->METIER."</td>
                            <td style='text-align:center;'>&nbsp;".$aventurier->NIVEAU."</td>  ";                          
                    echo "</tr>";
                    if($aventurier->ID > $maxID)
                    {
                        $maxID = $aventurier->ID;
                    }
                }
            ?>
        </table>
        <input type='hidden' value='<?php echo $maxID; ?>' name='maxID' />
        <div style='text-align:center;'><div style='text-align:center;'><input type='submit' value='Créer la compagnie' style='width:400px;'/></div></div><br>
    </form>
</div>
<script>
    function filtrer()
    {
        $( ".ligneAv" ).each(function( index ) 
            {                
                if(-1 != $(this).html().indexOf("<td>"+$("#filtre").val()))
                {
                    $(this).show();
                }
                else
                {
                    $(this).hide();
                }
            }
        );
    }
</script>